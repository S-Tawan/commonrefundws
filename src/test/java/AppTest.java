import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.util.PropertiesConfig;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;



/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }
    /**
     * Rigourous Test :-)
     * @throws InterfaceTLPROException 
     */
    public void testApp() throws CommonRefundException
    {
    	System.out.println("\n******************************************************\n");
    	
    	try (InputStream input = PropertiesConfig.class.getClassLoader().getResourceAsStream("config.properties")) {

			 Properties prop = new Properties();

			 if (input == null)
               System.err.println("Sorry, unable to find config.properties");

			 prop.load(input);
			 
			 prop.keySet();

			prop.forEach((k, v) -> System.out.println("Key : " + k + ", Value : " + v));
			 
		 } 
		 catch (IOException ex) {
			 ex.printStackTrace();
			 throw new CommonRefundException(ex);
		 }
    	
    	System.out.println("\n******************************************************\n");
    	
        assertTrue( true );
    }
}
