package com.thailife.refund.models;

import java.io.Serializable;

public class ResponseDataT<T extends Serializable> implements Serializable {

	/** 
	 *  
	 */
	private static final long serialVersionUID = 1L;

	private HeaderData headerData;

	private ResponseStatus responseStatus;

	private T responseRecord;

	public HeaderData getHeaderData() {
		return headerData;
	}

	public void setHeaderData(HeaderData headerData) {
		this.headerData = headerData;
	}

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public T getResponseRecord() {
		return responseRecord;
	}

	public void setResponseRecord(T responseRecord) {
		this.responseRecord = responseRecord;
	}

}
