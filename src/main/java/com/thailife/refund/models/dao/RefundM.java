package com.thailife.refund.models.dao;


import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.util.AESUtils;
import com.thailife.refund.util.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class RefundM implements Serializable {
    private static final long serialVersionUID = 1L;

    private int refundId;
    private String applicationId;
    private String prename;
    private String firstname;
    private String lastname;
    private String identifyId;
    private String agentId;
    private String policyNo;
    private String tempPolicyNo;
    private String paymentType;
    private String paymentTypeDesc;
    private String tempPaymentSlipNo;
    private String token;
    private String smsToken;
    private String mobileNo;
    private String creditCardNo;
    private String channel;
    private String applicationStatus;
    private String status;
    private String refundType;
    private String fullName;
    private String bankCode;
    private String bankName;
    private String accountNumber;
    private String premiumRefund;
    private Timestamp generateDate;
    private Timestamp submitDate;
    private Timestamp createDate;
    private Timestamp updateDate;
    private static final String secret = "CCS8720CCS872CCS";
    public RefundM() {
    }

    public RefundM(ResultSet resultSet) throws SQLException {
        this.refundId = resultSet.getInt("refund_id");
        this.applicationId = resultSet.getString("application_id");
        this.token = resultSet.getString("token");
//        this.smsToken = resultSet.getString("sms_token");
        this.prename = resultSet.getString("prename");
        this.firstname = resultSet.getString("firstname");
        this.lastname = resultSet.getString("lastname");
        this.identifyId = resultSet.getString("identify_id");
        this.agentId = resultSet.getString("agent_id");
        this.policyNo = resultSet.getString("policy_no");
        this.tempPolicyNo = resultSet.getString("temp_policy_no");
        this.paymentType = resultSet.getString("payment_type");
        this.paymentTypeDesc = resultSet.getString("payment_type_desc");
        this.tempPaymentSlipNo = resultSet.getString("temp_payment_slip_no");
        this.mobileNo = resultSet.getString("mobile_no");
        this.creditCardNo = resultSet.getString("credit_card_no");
        this.channel = resultSet.getString("channel");
        this.status = resultSet.getString("status");
        this.applicationStatus = resultSet.getString("application_status");
        this.refundType = resultSet.getString("refund_type");
        this.fullName = resultSet.getString("full_name");
        this.bankCode = resultSet.getString("bank_code");
        this.accountNumber = resultSet.getString("account_number");
        this.generateDate = resultSet.getTimestamp("generate_date");
        this.submitDate = resultSet.getTimestamp("submit_date");
        this.premiumRefund = resultSet.getString("premium_refund");
//        this.createDate = resultSet.getTimestamp("create_date");
//        this.updateDate = resultSet.getTimestamp("update_date");
    }


    public static RefundM convertFromJSONEncrypt(String strToDecrypt) throws Exception {
    	
    	if (StringUtils.isNull(strToDecrypt)) {
    		throw new CommonRefundException("strToDecrypt is null");
		}

        String decrypt = AESUtils.decrypt(secret,strToDecrypt).getData();

        if (StringUtils.isNull(decrypt)) {
			throw new CommonRefundException("decrypt is null : strToDecrypt = " + strToDecrypt);
        }
        
        return new ObjectMapper().readValue(decrypt, RefundM.class);
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getIdentifyId() {
        return identifyId;
    }

    public void setIdentifyId(String identifyId) {
        this.identifyId = identifyId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getTempPolicyNo() {
        return tempPolicyNo;
    }

    public void setTempPolicyNo(String tempPolicyNo) {
        this.tempPolicyNo = tempPolicyNo;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTypeDesc() {
        return paymentTypeDesc;
    }

    public void setPaymentTypeDesc(String paymentTypeDesc) {
        this.paymentTypeDesc = paymentTypeDesc;
    }

    public String getTempPaymentSlipNo() {
        return tempPaymentSlipNo;
    }

    public void setTempPaymentSlipNo(String tempPaymentSlipNo) {
        this.tempPaymentSlipNo = tempPaymentSlipNo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCreditCardNo() {
        return creditCardNo;
    }

    public void setCreditCardNo(String creditCardNo) {
        this.creditCardNo = creditCardNo;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Timestamp getGenerateDate() {
        return generateDate;
    }

    public void setGenerateDate(Timestamp generateDate) {
        this.generateDate = generateDate;
    }

    public  Timestamp getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Timestamp submitDate) {
        this.submitDate = submitDate;
    }

    public String getSmsToken() {
        return smsToken;
    }

    public void setSmsToken(String smsToken) {
        this.smsToken = smsToken;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getPremiumRefund() {
        return premiumRefund;
    }

    public void setPremiumRefund(String premiumRefund) {
        this.premiumRefund = premiumRefund;
    }
}
