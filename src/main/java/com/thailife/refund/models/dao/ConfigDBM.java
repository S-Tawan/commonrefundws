package com.thailife.refund.models.dao;

import java.io.Serializable;

public class ConfigDBM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private java.lang.String applicationName;

	private java.lang.String hostName;

	private java.lang.String portNo;

	private java.lang.String databaseName;

	private java.lang.String userName;

	private java.lang.String password;

	public java.lang.String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(java.lang.String applicationName) {
		this.applicationName = applicationName;
	}

	public java.lang.String getHostName() {
		return hostName;
	}

	public void setHostName(java.lang.String hostName) {
		this.hostName = hostName;
	}

	public java.lang.String getPortNo() {
		return portNo;
	}

	public void setPortNo(java.lang.String portNo) {
		this.portNo = portNo;
	}

	public java.lang.String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(java.lang.String databaseName) {
		this.databaseName = databaseName;
	}

	public java.lang.String getUserName() {
		return userName;
	}

	public void setUserName(java.lang.String userName) {
		this.userName = userName;
	}

	public java.lang.String getPassword() {
		return password;
	}

	public void setPassword(java.lang.String password) {
		this.password = password;
	}

}
