package com.thailife.refund.models.dao;


import java.io.Serializable;
import java.sql.Timestamp;

public class RefundDetailM implements Serializable {
    private static final long serialVersionUID = 1L;

    private int refundId;
    private RefundM refund;
    private String refundType;
    private String fullName;
    private String bank;
    private String accountNumber;
    private Timestamp createDate;
    private Timestamp updateDate;

    public int getRefundId() {
        return refundId;
    }

    public void setRefundId(int refundId) {
        this.refundId = refundId;
    }

    public RefundM getRefund() {
        return refund;
    }

    public void setRefund(RefundM refund) {
        this.refund = refund;
    }

    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }
}
