package com.thailife.refund.models.dao;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class OTPTransactionM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String tempslipno;
	
	private String reference;
	
	private String mobileno;
	
	private Timestamp createdate;
	
	private Timestamp expiredate;
	
	private Timestamp verifydate;
	
	private String otpcode;
	
	private String pid;
	
	private String identityid;
	
	private String verifyresult;

	public OTPTransactionM(ResultSet resultSet) throws SQLException {
		
		this.tempslipno = resultSet.getString("tempslipno");
		this.reference = resultSet.getString("reference");
		this.mobileno = resultSet.getString("mobileno");
		
		this.createdate = resultSet.getTimestamp("createdate");
		this.expiredate = resultSet.getTimestamp("expiredate");
		this.verifydate = resultSet.getTimestamp("verifydate");
		
		this.otpcode = resultSet.getString("otpcode");
		this.pid = resultSet.getString("pid");
		this.identityid = resultSet.getString("identityid");
		this.verifyresult = resultSet.getString("verifyresult");
	}

	public OTPTransactionM() {
		// TODO Auto-generated constructor stub
	}

	public String getTempslipno() {
		return tempslipno;
	}

	public void setTempslipno(String tempslipno) {
		this.tempslipno = tempslipno;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public Timestamp getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public Timestamp getExpiredate() {
		return expiredate;
	}

	public void setExpiredate(Timestamp expiredate) {
		this.expiredate = expiredate;
	}

	public Timestamp getVerifydate() {
		return verifydate;
	}

	public void setVerifydate(Timestamp verifydate) {
		this.verifydate = verifydate;
	}

	public String getOtpcode() {
		return otpcode;
	}

	public void setOtpcode(String otpcode) {
		this.otpcode = otpcode;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getIdentityid() {
		return identityid;
	}

	public void setIdentityid(String identityid) {
		this.identityid = identityid;
	}

	public String getVerifyresult() {
		return verifyresult;
	}

	public void setVerifyresult(String verifyresult) {
		this.verifyresult = verifyresult;
	}
	
	
}
