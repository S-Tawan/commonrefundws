package com.thailife.refund.models.dao;

import java.io.Serializable;

public class LogDetailM implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String seq = "";
	private String temprpno = "";
	private String applicationid = "";
	private String createdate = "";
	private String detail = "";
	
	private String url = "";
	private String request = "";
	private String response = "";
	private String typeslog = "";
	
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getTemprpno() {
		return temprpno;
	}
	public void setTemprpno(String temprpno) {
		this.temprpno = temprpno;
	}
	public String getApplicationid() {
		return applicationid;
	}
	public void setApplicationid(String applicationid) {
		this.applicationid = applicationid;
	}
	public String getCreatedate() {
		return createdate;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getTypeslog() {
		return typeslog;
	}
	public void setTypeslog(String typeslog) {
		this.typeslog = typeslog;
	}

}
