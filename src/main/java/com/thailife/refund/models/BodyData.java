package com.thailife.refund.models;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "body")
@XmlRootElement(name = "body")
public class BodyData<T extends Serializable> implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String agentid;
	private String aID;

	private String searchkey;

	/**
	 * Enum SearchMode
	 */
	private String searchmode;

	private String keyvalue;

	/**
	 * Enum ActionQuery
	 */
	private String action;

	private String lastsync;

	private int pageNo;

	private int pageSize;

	private String orderBy;

	private String orderType;

	private int size;

	private int sizeTotal;

	private int totalPage;

	private T data;

	private List<T> datas;

	private String sourcePage;

	private String filterBy;
	
	private Boolean pagingMode = Boolean.FALSE.booleanValue();

	public String getFilterBy() {
		return filterBy;
	}

	public void setFilterBy(String filterBy) {
		this.filterBy = filterBy;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getSizeTotal() {
		return sizeTotal;
	}

	public void setSizeTotal(int sizeTotal) {
		this.sizeTotal = sizeTotal;
	}

	public Boolean getPagingMode() {
		return pagingMode;
	}

	public void setPagingMode(Boolean pagingMode) {
		this.pagingMode = pagingMode;
	}

	public String getSourcePage() {
		return sourcePage;
	}

	public void setSourcePage(String sourcePage) {
		this.sourcePage = sourcePage;
	}

	public String getSearchkey() {
		return searchkey;
	}

	public void setSearchkey(String searchkey) {
		this.searchkey = searchkey;
	}

	public String getKeyvalue() {
		return keyvalue;
	}

	public void setKeyvalue(String keyvalue) {
		this.keyvalue = keyvalue;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<T> getDatas() {
		return datas;
	}

	public void setDatas(List<T> datas) {
		this.datas = datas;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getAgentid() {
		return agentid;
	}

	public void setAgentid(String agentid) {
		this.agentid = agentid;
	}

	public String getLastsync() {
		return lastsync;
	}

	public void setLastsync(String lastsync) {
		this.lastsync = lastsync;
	}

	public String getSearchmode() {
		return searchmode;
	}

	public void setSearchmode(String searchmode) {
		this.searchmode = searchmode;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getaID() {
		return aID;
	}

	public void setaID(String aID) {
		this.aID = aID;
	}

}

