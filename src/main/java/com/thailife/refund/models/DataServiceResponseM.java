package com.thailife.refund.models;

import java.io.Serializable;

public class DataServiceResponseM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HeaderData headers;
	
	private ResponseStatus status;

	public HeaderData getHeaders() {
		return headers;
	}

	public void setHeaders(HeaderData headers) {
		this.headers = headers;
	}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}



		
}
