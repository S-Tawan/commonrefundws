package com.thailife.refund.models;

import java.io.Serializable;

public class RequestParameterM<T extends Serializable> implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HeaderData headers;
	
	private BodyData<T> body;

	public HeaderData getHeaders() {
		return headers;
	}

	public void setHeaders(HeaderData headers) {
		this.headers = headers;
	}

	public BodyData<T> getBody() {
		return body;
	}

	public void setBody(BodyData<T> body) {
		this.body = body;
	}

	

}
