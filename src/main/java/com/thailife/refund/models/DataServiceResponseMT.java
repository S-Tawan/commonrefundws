package com.thailife.refund.models;

import java.io.Serializable;

public class DataServiceResponseMT<T extends Serializable> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HeaderData headers;
	
	private ResponseStatus status;
	
	private BodyResponseM<T> body;
	
	public HeaderData getHeaders() {
		return headers;
	}

	public void setHeaders(HeaderData headers) {
		this.headers = headers;
	}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public BodyResponseM<T> getBody() {
		return body;
	}

	public void setBody(BodyResponseM<T> body) {
		this.body = body;
	}
	
}
