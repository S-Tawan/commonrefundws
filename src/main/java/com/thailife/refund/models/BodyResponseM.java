package com.thailife.refund.models;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "body")
@XmlRootElement(name = "body")
public class BodyResponseM<T extends Serializable> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int totalRecord;

    private int pageTotal;

    private int size;

    private List<T> datas;

    private T data;

    public BodyResponseM(T data) {
        this.data = data;
    }

    public BodyResponseM() {
    }


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(int pageTotal) {
        this.pageTotal = pageTotal;
    }


}
