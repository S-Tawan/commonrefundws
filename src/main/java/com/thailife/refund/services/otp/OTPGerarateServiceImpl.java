package com.thailife.refund.services.otp;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.models.dao.OTPTransactionM;
import com.thailife.refund.models.dao.RefundM;
import com.thailife.refund.util.ErrorTag;
import com.thailife.refund.util.PropertiesConfig;
import com.thailife.refund.util.RestClientUtil;
import com.thailife.refund.util.SimpleDateFormatUtil;
import com.thailife.refund.util.StringUtils;

@Path("/otp")
public class OTPGerarateServiceImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(OTPGerarateServiceImpl.class);

	public OTPTransactionM utilityRequestOTP(String reference, RefundM refundM, String mobile) throws CommonRefundException {
		
		int appid = 1;
		String groupcode = "475";
		
		if (StringUtils.isNull(reference)) {
			reference = randomString(6);
		}
		
		JSONObject headerData = new JSONObject();
		headerData.put("messageId", UUID.randomUUID().toString());
		headerData.put("sentDateTime", StringUtils.dateToString(new Date(), SimpleDateFormatUtil.getDateFORMATPOSTGRESQL()));
		headerData.put("responseDateTime", "");
		
		JSONObject requestRecord = new JSONObject();
		requestRecord.put("agentID", refundM.getAgentId());
		requestRecord.put("reference", reference);
		requestRecord.put("mobileNumber",  mobile);
		requestRecord.put("app_id", appid);
		requestRecord.put("group_code", groupcode);
		
		JSONObject reqBody = new JSONObject();
		reqBody.put("headerData", headerData);
		reqBody.put("requestRecord", requestRecord);
		
		String url = PropertiesConfig.getInstance().getIpPort10102() + "/communication/rest/utilityRequestOTP";
		Response response = RestClientUtil.post(reqBody.toString(), url);
		String responseMsg = response.readEntity(String.class);
		
		LOGGER.debug("==> responseMsg = {}", responseMsg);
		if (!StringUtils.isNull(responseMsg)) {
			
			JSONObject object = new JSONObject(responseMsg);
			JSONObject responseRecord = object.getJSONObject("responseRecord");
			JSONObject responseStatus = object.getJSONObject("responseStatus");
			
			OTPTransactionM otpTransactionM = new OTPTransactionM();
			otpTransactionM.setExpiredate(Timestamp.valueOf(responseRecord.getString("expireDate")));
			otpTransactionM.setIdentityid(Integer.toString(responseRecord.getInt("identityID")));
			otpTransactionM.setOtpcode(responseRecord.getString("otp"));
			otpTransactionM.setPid(responseRecord.getString("pid"));
			otpTransactionM.setReference(responseRecord.getString("reference"));
			
			return otpTransactionM;
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param lengthOfOTP
	 * @return
	 */
	public static String genrateOTP(final int lengthOfOTP) {
		
		StringBuilder generatedOTP = new StringBuilder();
		SecureRandom secureRandom = new SecureRandom();
		try {
			secureRandom = SecureRandom.getInstance(secureRandom.getAlgorithm());
			for (int i = 0; i < lengthOfOTP; i++) {
				generatedOTP.append(secureRandom.nextInt(9));
			}
		} 
		catch (NoSuchAlgorithmException e) {
			LOGGER.error(ErrorTag.ERROR.getValue(), e.getMessage(), e);
		}
		
		return generatedOTP.toString();
	}
	
	public static String randomString(int len) {

		String ab = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(ab.charAt(new SecureRandom().nextInt(ab.length())));

		return sb.toString();
	}

	public VerifyOTPM verifyOTP(OTPTransactionM otpTransactionM,
			RefundM refundM, String mobile, String otpcode) throws CommonRefundException {
		
		int appid = 1;
		String groupcode = "475";
		
		JSONObject headerData = new JSONObject();
		headerData.put("messageId", UUID.randomUUID().toString());
		headerData.put("sentDateTime", StringUtils.dateToString(new Date(), SimpleDateFormatUtil.getDateFORMATPOSTGRESQL()));
		headerData.put("responseDateTime", "");
		
		JSONObject requestRecord = new JSONObject();
		requestRecord.put("identityID", otpTransactionM.getIdentityid());
		requestRecord.put("reference", otpTransactionM.getReference());
		requestRecord.put("agentID",  refundM.getAgentId());
		requestRecord.put("reference", otpTransactionM.getReference());
		requestRecord.put("mobileNumber", mobile);
		requestRecord.put("app_id", appid);
		requestRecord.put("group_code", groupcode);
		requestRecord.put("otpCode", otpcode);
		
		JSONObject reqBody = new JSONObject();
		reqBody.put("headerData", headerData);
		reqBody.put("requestRecord", requestRecord);
		
		String url = PropertiesConfig.getInstance().getIpPort10102() + "/communication/rest/utilityVerifyOTP";
		Response response = RestClientUtil.post(reqBody.toString(), url);
		String responseMsg = response.readEntity(String.class);
		
		LOGGER.debug("==> responseMsg = {}", responseMsg);
		if (!StringUtils.isNull(responseMsg)) {
			JSONObject object = new JSONObject(responseMsg);
			JSONObject responseRecord = object.getJSONObject("responseRecord");
			
			VerifyOTPM verifyOTPM = new VerifyOTPM();
			verifyOTPM.setMsgResult(responseRecord.getString("msgResult"));
			verifyOTPM.setVerifyResult(responseRecord.getString("verifyResult"));
			
			return verifyOTPM;
		}
		
		return null;
	}

}
