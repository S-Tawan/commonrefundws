package com.thailife.refund.services.otp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.models.dao.OTPTransactionM;

public class OTPTransactionDAOImpl {

	public OTPTransactionM findByTempPaymentSlipNo(String tempslipno,
			Connection connection) throws SQLException {
		
		String sql = "SELECT * FROM common_refund.otp_transaction WHERE tempslipno = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, tempslipno);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return new OTPTransactionM(resultSet);
                }
            }
        }
        
        return null;
		
	}

	public void insert(OTPTransactionM otpTransactionM,
			Connection connection) throws SQLException {
		
        String sql = " INSERT INTO common_refund.otp_transaction (tempslipno," +
                " reference," +
                " mobileno," +
                " createdate," +
                " expiredate," +
                " otpcode," +
                " pid," +
                " identityid," +
                " verifyresult) " +
                "VALUES (?, ?, ?, NOW(), ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
        	
            int i = 1;
            ps.setString(i++, otpTransactionM.getTempslipno());
            ps.setString(i++, otpTransactionM.getReference());
            ps.setString(i++, otpTransactionM.getMobileno());
            ps.setTimestamp(i++, otpTransactionM.getExpiredate());
            ps.setString(i++, otpTransactionM.getOtpcode());
            ps.setString(i++, otpTransactionM.getPid());
            ps.setString(i++, otpTransactionM.getIdentityid());
            ps.setString(i++, otpTransactionM.getVerifyresult());
            ps.executeUpdate();
        }
	}

	public int update(Connection connection, OTPTransactionM otpTransactionM,
			String tempslipno) throws CommonRefundException {

        String sql = "UPDATE common_refund.otp_transaction " +
                "SET expiredate = ?, " +
                "    otpcode = ?, " +
                "    pid = ?, " +
                "    identityid = ? " +
                "WHERE tempslipno = ? ";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
        	
            int i = 1;
            ps.setTimestamp(i++, otpTransactionM.getExpiredate());
            ps.setString(i++, otpTransactionM.getOtpcode());
            ps.setString(i++, otpTransactionM.getPid());
            ps.setString(i++, otpTransactionM.getIdentityid());
            ps.setString(i++, tempslipno);

            return ps.executeUpdate();

        } 
        catch (Exception e) {
            throw new CommonRefundException(e);
        }
	}

	/**
	 * 
	 * @param connection
	 * @param tempslipno
	 * @return
	 * @throws CommonRefundException
	 */
	public int updatevVerifydate(Connection connection, String tempslipno) throws CommonRefundException {

		String sql = "UPDATE common_refund.otp_transaction SET verifydate = NOW(), verifyresult = 'Y' WHERE tempslipno = ? ";
		try (PreparedStatement ps = connection.prepareStatement(sql)) {

			ps.setString(1, tempslipno);

			return ps.executeUpdate();
		} catch (Exception e) {
			throw new CommonRefundException(e);
		}
	}

}
