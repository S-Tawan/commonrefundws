package com.thailife.refund.services.otp;

import java.io.Serializable;

public class GenarateOTPM implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String tempslipno = "";
	
	private String mobile = "";

	public String getTempslipno() {
		return tempslipno;
	}

	public void setTempslipno(String tempslipno) {
		this.tempslipno = tempslipno;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}
