package com.thailife.refund.services.otp;

import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thailife.refund.connection.DataBaseConnector;
import com.thailife.refund.constant.StatusCodeType;
import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.models.DataServiceResponseMT;
import com.thailife.refund.models.dao.OTPTransactionM;
import com.thailife.refund.models.dao.RefundM;
import com.thailife.refund.services.refund.RefundDAOImpl;
import com.thailife.refund.util.RestClientUtil;
import com.thailife.refund.util.StringUtils;

@Path("/otp")
public class OTPGerarateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OTPGerarateService.class);
	
	private RefundDAOImpl refundDAOImpl = new RefundDAOImpl();
	
	private OTPTransactionDAOImpl oTPTransactionDAOImpl = new OTPTransactionDAOImpl();

	private OTPGerarateServiceImpl otpGerarateServiceImpl = new OTPGerarateServiceImpl();
	
	 /**
	  * utilityRequestOTP
	  * http://10.102.60.20:8080/communication/rest/utilityRequestOTP
	  * https://docs.google.com/spreadsheets/d/1F80dwVVhhp2aCn6tLxH7Joww-smVwQ9ZFG7LZ_GK3oc/edit#gid=1681781734
	  * @param requestM
	  * @return
	  * @throws Exception
	  */
	@GET
    @Path("/genarateOTP/{token}/{mobile}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataServiceResponseMT<OTPTransactionM> genarateOTP(@PathParam("token") String token, @PathParam("mobile") String mobile) throws CommonRefundException, SQLException {
	
		if (StringUtils.isNull(token))
            throw new CommonRefundException("token is null");
		
		if (StringUtils.isNull(mobile))
            throw new CommonRefundException("mobile is null");
		
		LOGGER.debug("genarateOTP : token = {}, mobile = {}", token, mobile);
		
		try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
			connection.setAutoCommit(false);
			
			/**
			 * TODO findByTempPaymentSlipNo
			 */
			RefundM refundM = refundDAOImpl.findByToken(token, connection);
			if (refundM == null) {
				LOGGER.debug("refundM is null.");
        		return RestClientUtil.returnResponse(StatusCodeType.E.toString(), "findByToken is null.", null);
			}
			
			LOGGER.debug("==> genarateOTP, getTempPaymentSlipNo = {}, getAgentId= {}", refundM.getTempPaymentSlipNo(), refundM.getAgentId());
			
			String reference = "";
			OTPTransactionM otpTransactionM = oTPTransactionDAOImpl.findByTempPaymentSlipNo(refundM.getTempPaymentSlipNo(), connection);
			if (otpTransactionM != null) {
				reference = otpTransactionM.getReference();
			}
			
			OTPTransactionM otpTransactionM2 = otpGerarateServiceImpl.utilityRequestOTP(reference, refundM, mobile);
			
			if (otpTransactionM == null) {
				
				/**
				 * Insert to table otp_transaction.
				 */
				otpTransactionM2.setTempslipno(refundM.getTempPaymentSlipNo());
				otpTransactionM2.setMobileno(mobile);
				oTPTransactionDAOImpl.insert(otpTransactionM2, connection);
			}
			else {
				/**
				 * Update table otp_transaction.
				 */
				oTPTransactionDAOImpl.update(connection, otpTransactionM2, refundM.getTempPaymentSlipNo());
			}
			
			connection.commit();
			
			otpTransactionM2.setOtpcode("");
			return RestClientUtil.returnResponse(StatusCodeType.S.toString(), "", otpTransactionM2);
		}
	 }
	
	@POST
	@Path("verifyOTP")
	@Produces({MediaType.APPLICATION_JSON})
	public DataServiceResponseMT<VerifyOTPM> verifyOTP(String request)
			throws Exception {

		if (StringUtils.isNull(request))
            throw new CommonRefundException("request is null");
		
		JSONObject object = new JSONObject(request);
		String token = object.getString("token");
		String mobile = object.getString("mobile");
		String otpcode = object.getString("otpcode");
		
		if (StringUtils.isNull(token))
            throw new CommonRefundException("token is null");
		
		if (StringUtils.isNull(mobile))
            throw new CommonRefundException("mobile is null");
		
		if (StringUtils.isNull(otpcode))
            throw new CommonRefundException("otpcode is null");
		
		try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
			connection.setAutoCommit(false);
			
			RefundM refundM = refundDAOImpl.findByToken(token, connection);
			if (refundM == null) {
				LOGGER.debug("refundM is null.");
	    		return RestClientUtil.returnResponse(StatusCodeType.E.toString(), "findByToken is null.", null);
			}
			
			OTPTransactionM otpTransactionM = oTPTransactionDAOImpl.findByTempPaymentSlipNo(refundM.getTempPaymentSlipNo(), connection);
			
			/**
			 * Call Verify OTP.
			 */
			VerifyOTPM verifyOTPM = otpGerarateServiceImpl.verifyOTP(otpTransactionM, refundM, mobile, otpcode);
			if ("Y".equalsIgnoreCase(verifyOTPM.getVerifyResult())) {
				
				oTPTransactionDAOImpl.updatevVerifydate(connection, refundM.getTempPaymentSlipNo());
				
				refundDAOImpl.updateMobilenoBytoken(connection, token, mobile);
				connection.commit();
			}
			
    		return RestClientUtil.returnResponse(StatusCodeType.S.toString(), "", verifyOTPM);
		}
	}

}
