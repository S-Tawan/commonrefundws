package com.thailife.refund.services.otp;

import java.io.Serializable;

public class VerifyOTPM implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String msgResult;
	
	private String verifyResult;

	public String getMsgResult() {
		return msgResult;
	}

	public void setMsgResult(String msgResult) {
		this.msgResult = msgResult;
	}

	public String getVerifyResult() {
		return verifyResult;
	}

	public void setVerifyResult(String verifyResult) {
		this.verifyResult = verifyResult;
	}

}
