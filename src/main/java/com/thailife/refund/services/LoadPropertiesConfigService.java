package com.thailife.refund.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.thailife.refund.exception.CommonRefundException;

@Path("/load")
public class LoadPropertiesConfigService {
	
	@GET
    @Path("/propertiesConfigService")
    @Produces({ MediaType.APPLICATION_JSON })
    public Map<String, String> propertiesConfigService() throws CommonRefundException {

		try (InputStream input = WebConfig.class.getClassLoader().getResourceAsStream("config.properties")) {

			Properties prop = new Properties();
   			Map<String, String> items = new HashMap<>();

   			if (input == null)
   				throw new CommonRefundException("Sorry, unable to find config.properties");

   			prop.load(input);
   			prop.keySet();
   			prop.forEach((k, v) -> items.put((String)k, (String)v) );
   			 
   			return items;
		} 
		catch (IOException ex) {
			throw new CommonRefundException(ex);
		}
    }

}
