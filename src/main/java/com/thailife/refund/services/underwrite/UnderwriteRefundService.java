package com.thailife.refund.services.underwrite;

import java.util.Date;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.util.ErrorTag;
import com.thailife.refund.util.PropertiesConfig;
import com.thailife.refund.util.RestClientUtil;
import com.thailife.refund.util.SimpleDateFormatUtil;
import com.thailife.refund.util.StringUtils;

/**
 * https://docs.google.com/spreadsheets/d/1F80dwVVhhp2aCn6tLxH7Joww-smVwQ9ZFG7LZ_GK3oc/edit#gid=298337599
 * @author tleuser
 *
 */
@Path("/underwrite")
public class UnderwriteRefundService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UnderwriteRefundService.class);

    @POST
    @Path("/coreRefundService")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public CoreRefundM coreRefundService(CoreRefundM req) throws Exception {
    	
    	try {
    		String url = PropertiesConfig.getInstance().getIpPort10102();
    		url += "/CoreRefundRestWS/rest/refund/refuse";
    		
    		req.setUrl(url);
    		
    		JSONObject header = new JSONObject();
    		header.put("messageId", UUID.randomUUID().toString());
    		header.put("sentDateTime", StringUtils.dateToString(new Date(), SimpleDateFormatUtil.getDateFormatDDMMYYYTHHMMSS()));
    		header.put("responseDateTime", "");
    		
    		JSONObject requestRecord = new JSONObject();
    		requestRecord.put("tempPolicyNo", "TB013254");
    		requestRecord.put("forBranch", "947");
    		requestRecord.put("refundType", "T");
    		requestRecord.put("bankCode", "002");
    		requestRecord.put("bankName", "ธนาคารกรุงเทพ");
    		requestRecord.put("accountNo", "1234567890");
    		requestRecord.put("accountName", "ทดสอบคืนเงิน ปฏิเสธรับประกัน");
    		requestRecord.put("mobilePhone", "0801234567");
    		
    		JSONObject request = new JSONObject();
    		request.put("headerData", header);
    		request.put("requestRecord", requestRecord);
    		
    		Response response = RestClientUtil.post(request.toString(), url);
    		String msg = response.readEntity(String.class);
    		
    		LOGGER.debug("==> debug {}", msg);
    		LOGGER.error("==> error {}", msg);
    	
    	} 
    	catch (Exception e) {
    		LOGGER.error(ErrorTag.ERROR.getValue(), e.getMessage(), e);
			throw new CommonRefundException(e);
		}
    	
		return req;
    }

}
