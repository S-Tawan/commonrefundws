package com.thailife.refund.services.refund;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thailife.refund.constant.StatusCodeType;
import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.models.SaleinfomationM;
import com.thailife.refund.util.PropertiesConfig;
import com.thailife.refund.util.RestClientUtil;
import com.thailife.refund.util.SimpleDateFormatUtil;
import com.thailife.refund.util.StringUtils;

public class RefundServiceImpl {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RefundServiceImpl.class);

	/**
	 * ข้อมูลตัวแทน
	 * @param agentId
	 * @return
	 * @throws CommonRefundException
	 */
	public SaleinfomationM getSaleinfomation(String agentId) throws CommonRefundException {
	
		JSONObject req = new JSONObject();
		req.put("salesID", agentId);
		
		String url = PropertiesConfig.getInstance().getIpPortTLPro() + "/TLPromptBackendWs/rest/saleinfo/searchSaleinfoBySaleidService";
		Response response = RestClientUtil.post(req.toString(), url);
		
		String resMsg = response.readEntity(String.class);
		if (!StringUtils.isNull(resMsg)) {
			
			JSONObject object = new JSONObject(resMsg);
			
			SaleinfomationM saleinfomationM = new SaleinfomationM();
			saleinfomationM.setFirstname(object.getString("firstname"));
			saleinfomationM.setLastname(object.getString("lastname"));
			saleinfomationM.setMobileno(object.getString("mobileno"));
			
			return saleinfomationM;
		}
		
		return null;
	}

	/**
	 * check เบอร์โทร ซ้ำกับตัวแทนปัจจุบัน			
		REC-15			
	 * https://docs.google.com/spreadsheets/d/1F80dwVVhhp2aCn6tLxH7Joww-smVwQ9ZFG7LZ_GK3oc/edit#gid=1923364939
	 * @param mobile
	 * @return
	 * @throws CommonRefundException 
	 */
	public List<SaleinfomationM> checkDupMobileno(String mobile) throws CommonRefundException {
		
		List<SaleinfomationM> saleinfomationMs = new ArrayList<>();
		
		JSONObject headerData = new JSONObject();
		headerData.put("messageId", UUID.randomUUID().toString());
		headerData.put("sentDateTime", StringUtils.dateToString(new Date(), SimpleDateFormatUtil.getDateFORMATPOSTGRESQL()));
		headerData.put("responseDateTime", "");
		
		JSONObject requestRecord = new JSONObject();
		requestRecord.put("phonenumber", mobile);
		
		JSONObject req = new JSONObject();
		req.put("headerData", headerData);
		req.put("requestRecord", requestRecord);
		
		String url = PropertiesConfig.getInstance().getIpPortGateWay10102() + "/wsPDPA/ws/rest/request/checkdupmobileno/1.0";
		Response response = RestClientUtil.post(req.toString(), url);
		String msg = response.readEntity(String.class);
		LOGGER.debug("checkDupMobileno : msg = {}", msg);
		if (!StringUtils.isNull(msg)) {
			
			JSONObject jsonObject = new JSONObject(msg);
			JSONObject responseStatus = jsonObject.getJSONObject("responseStatus");
			String status = responseStatus.getString("status");
			
			boolean hasRecord = false;
			JSONArray agentList = null;
			
			LOGGER.debug("status = {}", status);
			if (StatusCodeType.S.toString().equalsIgnoreCase(status)) {
				
				JSONObject responseRecord = jsonObject.getJSONObject("responseRecord");
				agentList = responseRecord.getJSONArray("agentList");
				LOGGER.debug("agentList = {}, length = {}", agentList, agentList.length());
				hasRecord = agentList.length() > 0;
			}
			
			if (hasRecord) {
				
				for (int i = 0; i < agentList.length(); i++) {
					
					JSONObject object = agentList.getJSONObject(i);
					String firstname = object.getString("firstname");
					String lastname = object.getString("lastname");
					
					SaleinfomationM saleinfomationM = new SaleinfomationM();
					saleinfomationM.setFirstname(firstname);
					saleinfomationM.setLastname(lastname);
					LOGGER.debug("firstname = {}, lastname = {}", firstname, lastname);
					
					saleinfomationMs.add(saleinfomationM);
				}
				
				return saleinfomationMs;
			}
		}
		
		return saleinfomationMs;
	}

}
