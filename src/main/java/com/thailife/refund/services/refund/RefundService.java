package com.thailife.refund.services.refund;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thailife.refund.connection.DataBaseConnector;
import com.thailife.refund.constant.EnvType;
import com.thailife.refund.constant.RefundStatus;
import com.thailife.refund.constant.StatusCodeType;
import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.models.BodyResponseM;
import com.thailife.refund.models.DataServiceResponseMT;
import com.thailife.refund.models.HeaderData;
import com.thailife.refund.models.RequestParameterM;
import com.thailife.refund.models.ResponseStatus;
import com.thailife.refund.models.SaleinfomationM;
import com.thailife.refund.models.dao.ConfigDBM;
import com.thailife.refund.models.dao.LogDetailM;
import com.thailife.refund.models.dao.RefundM;
import com.thailife.refund.models.dao.RefundMV2;
import com.thailife.refund.services.underwrite.CoreRefundM;
import com.thailife.refund.util.AESUtils;
import com.thailife.refund.util.ErrorTag;
import com.thailife.refund.util.PropertiesConfig;
import com.thailife.refund.util.RestClientUtil;
import com.thailife.refund.util.SimpleDateFormatUtil;
import com.thailife.refund.util.StringUtils;

import utility.database.ConnectionBean;
import utility.database.DBConnectionFile;

@Path("/refund")
public class RefundService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RefundService.class);

    private static final String STATUS_SUCCESS = "SUCCESS";

    private static final String ERRORCODE_SUCCESS = "S";

    private static final int MINUTE_TIMEOUT_30 = 30;

    private static RefundDAOImpl refundDAOImpl = new RefundDAOImpl();
    
    private static RefundServiceImpl refundServiceImpl = new RefundServiceImpl();
    
    private static LogRefundDetail logRefundDetail = new LogRefundDetail();

    @POST
    @Path("/checkDisableBtn")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataServiceResponseMT<String> printMessage(RequestParameterM<String> req) throws CommonRefundException, SQLException, IOException {
        List<String> datas = req.getBody().getDatas();
        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {

            Map<String, Boolean> booleans = refundDAOImpl.checkDisableBtnByApplicationId(datas, MINUTE_TIMEOUT_30, connection);

            JSONObject map = new JSONObject(booleans);
            ResponseStatus responseStatus = new ResponseStatus();
            responseStatus.setStatus(STATUS_SUCCESS);
            responseStatus.setErrorCode(ERRORCODE_SUCCESS);

            HeaderData headerData = req.getHeaders() != null ? req.getHeaders() : new HeaderData();
            headerData.setResponseDateTime(SimpleDateFormatUtil.getDateFormatHeader().format(new Date()));

            DataServiceResponseMT<String> stringResponseDataT = new DataServiceResponseMT<>();
            BodyResponseM<String> refundMBodyResponseM = new BodyResponseM<>(map.toString());
            stringResponseDataT.setBody(refundMBodyResponseM);
            stringResponseDataT.setStatus(responseStatus);
            stringResponseDataT.setHeaders(headerData);

            return stringResponseDataT;
        }
    }

    @POST
    @Path("/token")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataServiceResponseMT<RefundM> token(RequestParameterM<String> req) throws Exception {

        String encrypt = req.getBody().getData();

        LOGGER.debug("Request encrypt = " + encrypt);
        RefundM cheryPick = RefundM.convertFromJSONEncrypt(encrypt);

        String secretKey = "CCS8720CCS872CCS";
        String token = AESUtils.encrypt(secretKey, cheryPick.getApplicationId() + LocalDateTime.now().hashCode());
        token = token.replaceAll("[^a-zA-Z0-9]", "");
        cheryPick.setToken(token);
        //savaData
        String tempPaymentSlipNo = cheryPick.getTempPaymentSlipNo();
        if (tempPaymentSlipNo == null)
            throw new CommonRefundException("TempPaymentSlipNo is Null");

        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
            connection.setAutoCommit(false);
            
            logRefundDetail.save(connection, tempPaymentSlipNo, cheryPick.getApplicationId(), "Create token, status = " + cheryPick.getApplicationStatus(), "", "", "", "GET_TOKEN");
            
            if (cheryPick.getApplicationStatus() == null) {
                
            	RefundM tmp = refundDAOImpl.findByTempPaymentSlipNoAndStatus(tempPaymentSlipNo, RefundStatus.REFUSE, connection);
                if (tmp == null)
                    tmp = refundDAOImpl.findByTempPaymentSlipNoAndStatus(tempPaymentSlipNo, RefundStatus.APPROVE, connection);

                if (tmp == null)
                    throw new CommonRefundException("ApplicationStatus is wrong");

                cheryPick.setApplicationStatus(tmp.getApplicationStatus());
            }

            RefundM refundSearch = refundDAOImpl.findByTempPaymentSlipNoAndStatus(tempPaymentSlipNo, RefundStatus.PENDING, connection);
            if (refundSearch != null) {
                refundDAOImpl.updateByTempPaymentSlipNo(cheryPick, tempPaymentSlipNo, connection);
            } 
            else {
                refundDAOImpl.insert(cheryPick, connection);
            }

            connection.commit();
            RefundM refundM = new RefundM();
            refundM.setToken(token);

            ResponseStatus responseStatus = new ResponseStatus();
            responseStatus.setStatus(STATUS_SUCCESS);
            responseStatus.setErrorCode(ERRORCODE_SUCCESS);

            HeaderData headerData = req.getHeaders() != null ? req.getHeaders() : new HeaderData();
            headerData.setResponseDateTime(SimpleDateFormatUtil.getDateFormatHeader().format(new Date()));

            DataServiceResponseMT<RefundM> stringResponseDataT = new DataServiceResponseMT<>();
            BodyResponseM<RefundM> refundMBodyResponseM = new BodyResponseM<>(refundM);
            stringResponseDataT.setBody(refundMBodyResponseM);
            stringResponseDataT.setStatus(responseStatus);
            stringResponseDataT.setHeaders(headerData);

            return stringResponseDataT;
        } 
        catch (CommonRefundException e) {
            LOGGER.error(ErrorTag.ERROR.getValue(), e.getMessage(), e);
            throw new CommonRefundException(e);
        }
    }

    /**
     * บันทึกข้อมูลบัญชี
     * @param requestM
     * @param token
     * @return
     * @throws CommonRefundException
     */
    @POST
    @Path("/{token}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataServiceResponseMT<RefundM> saveRefundDetail(RefundM requestM, @PathParam("token") String token) throws CommonRefundException {

        if (token == null) {
            throw new CommonRefundException("Token is Null");
        }

        boolean matches = Pattern.compile("\\d\\d\\d").matcher(requestM.getBankCode()).matches();

        if (requestM.getBankCode().length() != 3 || !matches) {
            throw new CommonRefundException("BankCode is wrong");
        }

        ResponseStatus responseStatus = new ResponseStatus();
        DataServiceResponseMT<RefundM> dataServiceResponseMT = new DataServiceResponseMT<>();
        BodyResponseM<RefundM> refundMBodyResponseM = new BodyResponseM<>();

        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
            connection.setAutoCommit(false);
            RefundM byToken = refundDAOImpl.findByToken(token, connection);
            if (byToken == null) {
                throw new CommonRefundException("Not Found Token");
            }

            LocalDateTime now = LocalDateTime.now();
            if (byToken.getSubmitDate() != null && byToken.getSubmitDate().toLocalDateTime().plusMinutes(MINUTE_TIMEOUT_30).isAfter(now)) {
                responseStatus.setStatus("ERROR");
                responseStatus.setErrorCode("Expired");
                responseStatus.setErrorMessage("TimeOut !");

                dataServiceResponseMT.setBody(refundMBodyResponseM);
                dataServiceResponseMT.setStatus(responseStatus);

                return dataServiceResponseMT;
            }

            requestM.setToken(token);
            requestM.setMobileNo(byToken.getMobileNo());
            String secretKey = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSS"));
            String smsToken = AESUtils.encrypt(secretKey, requestM.getMobileNo())
                    .replaceAll("[^a-zA-Z0-9]", "").substring(0, 8);
            requestM.setSmsToken(smsToken);
            
            int res = refundDAOImpl.updateDetailAccount(requestM, connection);
            if (res > 0) {
                responseStatus.setStatus(STATUS_SUCCESS);
                responseStatus.setErrorCode(ERRORCODE_SUCCESS);
                RefundM refundM = refundDAOImpl.findByToken(token, connection);
                refundMBodyResponseM.setData(refundM);

                //send SMS
                if (!EnvType.DEV.equals(PropertiesConfig.getInstance().getEnvType()) && !EnvType.SIT.equals(PropertiesConfig.getInstance().getEnvType())) {
                    refundM.setSmsToken(smsToken);
                    refundM.setBankName(refundDAOImpl.findBankNameByBankCode(refundM.getBankCode(), connection));
                    sendSMS(refundM);
                    refundM.setSmsToken(null);
                } 
                else {
                    refundM.setSmsToken(PropertiesConfig.getInstance().getCommonRefundWebview() + smsToken);
                }
            } 
            else {
                responseStatus.setStatus("ERROR");
                responseStatus.setErrorCode("E");
                responseStatus.setErrorMessage("Can't update this token");
            }
            
            dataServiceResponseMT.setBody(refundMBodyResponseM);
            dataServiceResponseMT.setStatus(responseStatus);
            connection.commit();
            return dataServiceResponseMT;
        } 
        catch (Exception e) {
            LOGGER.debug("Error {}",  e.getMessage());
            if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
            else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }

    private void sendSMS(RefundM refundM) throws CommonRefundException {
        String refundWebview = PropertiesConfig.getInstance().getCommonRefundWebview();
        String linkSMS = refundWebview + refundM.getSmsToken();
        String message = String.format(
                "กรุณาตรวจสอบข้อมูลคำขอคืนเบี้ยประกันภัย ของคุณ%s %s (%s)\n" +
                        "กรุณายืนยันข้อมูลภายในวันที่ %s เวลา %s น. ที่ลิ้งค์\n" +
                        "%s\n" +
                        "หรือสอบถามตัวแทนของท่าน",
                refundM.getFirstname(), refundM.getLastname(), 
                StringUtils.isNull(refundM.getPolicyNo()) ? refundM.getTempPolicyNo() : refundM.getPolicyNo(),
                LocalDate.now().plusYears(543).format(DateTimeFormatter.ofPattern("dd/MM/yy")),
                LocalTime.now().plusMinutes(MINUTE_TIMEOUT_30).format(DateTimeFormatter.ofPattern("HH:mm")),
                linkSMS);

        sendSMSAPI(refundM.getMobileNo(), message);
    }

    /**
     * เข้าหน้า Common Refund ครั้งแรก
     * @param token
     * @return
     * @throws CommonRefundException
     */
    @GET
    @Path("/{token}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataServiceResponseMT<RefundM> getRefundM(@PathParam("token") String token) throws CommonRefundException {
        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {

            RefundM refundM = new RefundM();
            token = token.replace(" ", "+");
            refundM = refundDAOImpl.findByToken(token, connection);

            ResponseStatus responseStatus = new ResponseStatus();
            DataServiceResponseMT<RefundM> dataServiceResponseMT = new DataServiceResponseMT<RefundM>();
            BodyResponseM<RefundM> refundMBodyResponseM = new BodyResponseM<>();

            if (refundM != null) {
            	
                LocalDateTime now = LocalDateTime.now();
                if (refundM.getGenerateDate().toLocalDateTime().plusMinutes(5).isAfter(now) 
                		&& (refundM.getSubmitDate() == null || !refundM.getSubmitDate().toLocalDateTime().plusMinutes(MINUTE_TIMEOUT_30).isAfter(now))) {
                    
                	responseStatus.setStatus(STATUS_SUCCESS);
                    responseStatus.setErrorCode(ERRORCODE_SUCCESS);
                    refundMBodyResponseM.setData(refundM);
                } 
                else {
                    responseStatus.setStatus("ERROR");
                    responseStatus.setErrorCode("Expired");
                    responseStatus.setErrorMessage("TimeOut !");
                }
            } 
            else {
                responseStatus.setStatus("ERROR");
                responseStatus.setErrorCode("E");
                responseStatus.setErrorMessage("Not Found Token");
            }
            
            dataServiceResponseMT.setBody(refundMBodyResponseM);
            dataServiceResponseMT.setStatus(responseStatus);

            return dataServiceResponseMT;
        } 
        catch (Exception e) {
            
        	if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
            else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }

    @GET
    @Path("/review/{smsToken}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataServiceResponseMT<RefundM> reviewSMSToken(@PathParam("smsToken") String smsToken) throws CommonRefundException {
        
    	String smsTokenOriginal = smsToken;
    	
    	try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {

            RefundM refundM = new RefundM();
            smsToken = smsToken.replace(" ", "+");
            refundM = refundDAOImpl.findBySMSToken(smsToken, connection);
            refundM.setBankName(refundDAOImpl.findBankNameByBankCode(refundM.getBankCode(), connection));

            ResponseStatus responseStatus = new ResponseStatus();
            DataServiceResponseMT<RefundM> dataServiceResponseMT = new DataServiceResponseMT<>();
            BodyResponseM<RefundM> refundMBodyResponseM = new BodyResponseM<>();

        	logRefundDetail.save(connection, refundM.getTempPaymentSlipNo(), refundM.getApplicationId(), 
        			"GET review smsToken = " + smsTokenOriginal, "", "getSubmitDate = " + refundM.getSubmitDate().toString(), "Status = " + refundM.getStatus(), "REVIEW");

            if (refundM.getSubmitDate() != null) {
            	
                LocalDateTime now = LocalDateTime.now();
                if (refundM.getSubmitDate().toLocalDateTime().plusMinutes(MINUTE_TIMEOUT_30).isAfter(now) && RefundStatus.PENDING.name().equalsIgnoreCase(refundM.getStatus())) {
                    responseStatus.setStatus(STATUS_SUCCESS);
                    responseStatus.setErrorCode(ERRORCODE_SUCCESS);
                    refundMBodyResponseM.setData(refundM);
                } 
                else {
                    responseStatus.setStatus("Expired");
                    responseStatus.setErrorCode("E");
                    responseStatus.setErrorMessage("TimeOut !");
                }
            } 
            else {
            	
                responseStatus.setStatus("ERROR");
                responseStatus.setErrorCode("E");
                responseStatus.setErrorMessage("Not Found Token");
            }
            
            dataServiceResponseMT.setBody(refundMBodyResponseM);
            dataServiceResponseMT.setStatus(responseStatus);

            return dataServiceResponseMT;
        } 
        catch (Exception e) {
            if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
            else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }

    /**
     * https://docs.google.com/spreadsheets/d/1F80dwVVhhp2aCn6tLxH7Joww-smVwQ9ZFG7LZ_GK3oc/edit#gid=298337599
     * ยืนยันข้อมูลบัญชี
     *
     * @param req
     * @return
     * @throws Exception
     */
    @POST
    @Path("review/{smsToken}")
    @Produces({MediaType.APPLICATION_JSON})
    public DataServiceResponseMT<CoreRefundM> confirmAccount(@PathParam("smsToken") String smsToken, String requestM) throws Exception {

        if (StringUtils.isNull(smsToken))
            throw new CommonRefundException("token is null");
        
        LOGGER.debug("==> confirmAccount : smsToken = {}, requestM = {}", smsToken, requestM);

        JSONObject jsonObject = new JSONObject(requestM);
        RefundStatus confirm;
        if (jsonObject.has("confirm")) {
            String confirm1 = jsonObject.getString("confirm");
            confirm = "Y".equalsIgnoreCase(confirm1) ?
                    RefundStatus.APPROVE :
                    RefundStatus.REFUSE;
        } 
        else {
            throw new CommonRefundException("confirm is null");
        }
        
        ResponseStatus responseStatus = new ResponseStatus();
        DataServiceResponseMT<CoreRefundM> dataServiceResponseMT = new DataServiceResponseMT<CoreRefundM>();
        BodyResponseM<CoreRefundM> refundMBodyResponseM = new BodyResponseM<>();
        CoreRefundM coreRefundM = new CoreRefundM();
        
        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
            connection.setAutoCommit(false);
            RefundM refundM = refundDAOImpl.findBySMSToken(smsToken, connection);
            if (refundM != null) {
            	
            	LOGGER.debug("==> confirmAccount : getApplicationId = {}, getTempPaymentSlipNo = {}", refundM.getApplicationId(), refundM.getTempPaymentSlipNo());

            	logRefundDetail.save(connection, refundM.getTempPaymentSlipNo(), refundM.getApplicationId(), "Confirm account, confirm = " + confirm.toString(), "", "", "", "REFUND");
            	
                if (refundM.getStatus().equalsIgnoreCase(RefundStatus.PENDING.getValue())) {
                    int res = refundDAOImpl.updateStatusBySmsToken(confirm.name(), smsToken, connection);
                    if (res > 0) {
                        if (confirm.equals(RefundStatus.APPROVE)) {
                        	
                        	 String bankname = refundDAOImpl.findBankNameByBankCode(refundM.getBankCode(), connection);
                             String message = String.format("บริษัทฯ ได้รับเลขที่บัญชีสำหรับโอนเบี้ยประกันภัยคืนเป็นที่เรียบร้อย (กรมธรรม์ชั่วคราวเลขที่ %s, คุณ%s %s, ธนาคาร%s, เลขที่บัญชี %s) หากมีข้อสงสัยกรุณาสอบถามตัวแทนของท่าน", 
                             		refundM.getTempPolicyNo(), refundM.getFirstname(), refundM.getLastname(), bankname.replace("ธ.", ""), refundM.getAccountNumber());
                             
                             sendSMSAPI(refundM.getMobileNo(), message);
                            /**
                             * DGT-36-01   (refuse เป็นเคสที่ ยกเลิก / เลื่อน / ปฏิเสธ  สถานะในPro+ คือ 30)
                             * https://docs.google.com/spreadsheets/d/1F80dwVVhhp2aCn6tLxH7Joww-smVwQ9ZFG7LZ_GK3oc/edit#gid=298337599
                             */
                             String url = PropertiesConfig.getInstance().getIpPort10102();
                             url += "/CoreRefundRestWS/rest/refund/" + refundM.getApplicationStatus().toLowerCase();

                             coreRefundM.setUrl(url);
                             coreRefundM.setConfirmStatus(confirm.name().toLowerCase());

                             JSONObject header = new JSONObject();
	                         header.put("messageId", UUID.randomUUID().toString());
	                         header.put("sentDateTime", StringUtils.dateToString(new Date(), SimpleDateFormatUtil.getDateFormatDDMMYYYTHHMMSS()));
	                         header.put("responseDateTime", "");

                             JSONObject requestRecord = new JSONObject();
                             requestRecord.put("tempPolicyNo", refundM.getTempPolicyNo());
                             requestRecord.put("forBranch", "947");
                             requestRecord.put("refundType", "T");
                             requestRecord.put("bankCode", refundM.getBankCode());
                             String bankName = refundDAOImpl.findBankNameByBankCode(refundM.getBankCode(), connection);
                             requestRecord.put("bankName", bankName);
                             requestRecord.put("accountNo", refundM.getAccountNumber());
                             requestRecord.put("accountName", refundM.getFullName());
                             requestRecord.put("mobilePhone", refundM.getMobileNo());

                             JSONObject request = new JSONObject();
                             request.put("headerData", header);
                             request.put("requestRecord", requestRecord);

                             Response response = RestClientUtil.post(request.toString(), url);
                             if (response.getStatus() == 200) {
                            	 String msg = response.readEntity(String.class);
                                 LOGGER.debug("Confirm account : getStatus = {}, msg ==> {}", response.getStatus(), msg);
                                 
                                 logRefundDetail.save(connection, refundM.getTempPaymentSlipNo(), refundM.getApplicationId(), "Confirm account, ยืนยันบัญชีคืนเงิน", url, "", msg, "REFUND");
                                
                                 JSONObject status = new JSONObject(msg).getJSONObject("responseStatus");
                                 String errorCode = status.getString("errorCode");
                                 if ("200".equalsIgnoreCase(errorCode)) {
                                    coreRefundM.setResponseMsg(msg);
                                    coreRefundM.setRequestMsg(request.toString());

                                    boolean result = refundDAOImpl.updateApplicationStatus(refundM.getTempPaymentSlipNo(), confirm.getValue(), msg, refundM.getChannel());
                                    if (result || PropertiesConfig.getInstance().getEnvType().equals(EnvType.DEV)) {
                                        
                                        logRefundDetail.save(connection, refundM.getTempPaymentSlipNo(), refundM.getApplicationId(), "Confirm account, Call to TLPRO+ [38, 39] Success.", "", "", "", "REFUND");
                                    	
                                    	coreRefundM.setResult(Boolean.toString(result));
                                        responseStatus.setStatus("SUCCESS");
                                        responseStatus.setErrorCode("S");
                                        refundMBodyResponseM.setData(coreRefundM);
                                        connection.commit();

                                        LOGGER.debug("==> debug {}", msg);
                                        LOGGER.error("==> error {}", msg);
                                        LOGGER.debug("==> result {}", result);
                                    } 
                                    else {
                                    	
                                        logRefundDetail.save(connection, refundM.getTempPaymentSlipNo(), refundM.getApplicationId(), "Confirm account, Call to TLPRO+ [38, 39] Fail.", "", "", "", "REFUND");
                                    	
                                        responseStatus.setStatus("ERROR");
                                        responseStatus.setErrorCode("E");
                                        responseStatus.setErrorMessage("TLPromptBackendWs : Fail");
                                        connection.commit();
                                    }
                                } 
                                else {
                                    responseStatus.setStatus("ERROR");
                                    responseStatus.setErrorCode("E");
                                    responseStatus.setErrorMessage("CoreRefundRestWS : " + status.getString("errorMessage"));
                                    connection.commit();
                                }
                            } 
                            else {
                            	
                            	String msg = response.readEntity(String.class);
                            	
                            	LOGGER.debug("Confirm account : getStatus = {}, msg ==> {}", response.getStatus(), msg);
                            	
                                responseStatus.setStatus("ERROR");
                                responseStatus.setErrorCode("E");
                                responseStatus.setErrorMessage("CoreRefundRestWS : " + msg);
                            }
                        } 
                        else {
                            
                        	boolean result = refundDAOImpl.updateApplicationStatus(refundM.getTempPaymentSlipNo(), confirm.getValue(), null, refundM.getChannel());
                            if (result || PropertiesConfig.getInstance().getEnvType().equals(EnvType.DEV)) {
                                coreRefundM.setResult(Boolean.toString(result));
                                responseStatus.setStatus("SUCCESS");
                                responseStatus.setErrorCode("S");
                                refundMBodyResponseM.setData(coreRefundM);
                                connection.commit();

                                LOGGER.debug("==> result {}", result);
                            } 
                            else {
                                responseStatus.setStatus("ERROR");
                                responseStatus.setErrorCode("E");
                                responseStatus.setErrorMessage("TLPromptBackendWs : Fail");
                            }
                        }
                    } 
                    else {
                        responseStatus.setStatus("ERROR");
                        responseStatus.setErrorCode("E");
                        responseStatus.setErrorMessage("Can't Update Token");
                    }
                } 
                else {
                    responseStatus.setStatus("ERROR");
                    responseStatus.setErrorCode("E");
                    responseStatus.setErrorMessage("Confirmed");
                }
            } 
            else {
                responseStatus.setStatus("ERROR");
                responseStatus.setErrorCode("E");
                responseStatus.setErrorMessage("Not Found Token");
            }
        } 
        catch (Exception e) {
            LOGGER.error(ErrorTag.ERROR.getValue(), e.getMessage(), e);
            throw new CommonRefundException(e);
        }
        
        dataServiceResponseMT.setBody(refundMBodyResponseM);
        dataServiceResponseMT.setStatus(responseStatus);
        return dataServiceResponseMT;
    }

    @GET
    @Path("/readDBConfig")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ConfigDBM readDBConfig() throws CommonRefundException {

        try {
            File fileEncrypted = new File("/c/resources/db/dbconfig.cfg");
            File encryptKey = new File("/c/resources/db/dbkey.cfg");

            LOGGER.debug("DataBaseConnector : fileEncrypted = {}", fileEncrypted.getAbsolutePath());

            DBConnectionFile fileDB = new DBConnectionFile(fileEncrypted, encryptKey, "DBCustomer");
            ConnectionBean connectionBean = fileDB.getConnectionBean();

            ConfigDBM configDBM = new ConfigDBM();
            configDBM.setApplicationName(connectionBean.getApplicationName());
            configDBM.setDatabaseName(connectionBean.getDatabaseName());
            configDBM.setHostName(connectionBean.getHostName());
            configDBM.setPassword(connectionBean.getPassword());
            configDBM.setPortNo(connectionBean.getPortNo());
            configDBM.setUserName(connectionBean.getUserName());

            return configDBM;
        } catch (Exception e) {
            LOGGER.error("DataBaseConnector ERROR : C");
            throw new CommonRefundException(e);
        }
    }

    @GET
    @Path("/sendSMS/{mobile}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public String sendSMS(@PathParam("mobile") String mobile) throws CommonRefundException {

        return sendSMSAPI(mobile, "Test SMS");

    }

    @GET
    @Path("/getDate")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public String getDate() throws CommonRefundException {
        return new Date().toString();
    }

    /**
     * This service for interfaceTLPROPlus. DGT-35
     * @param requestM
     * @return
     * @throws CommonRefundException
     */
    @POST
    @Path("/refundComplete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public String refundComplete(String requestM) throws CommonRefundException {

        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {

            JSONObject requestJSON = new JSONObject(requestM);

            if (!requestJSON.has("tempPolicyNo"))
                throw new CommonRefundException("tempPolicyNo is null");

            String tempPolicyNo = requestJSON.getString("tempPolicyNo");

            RefundM refundM = refundDAOImpl.findByTempPolicyNoOrPolicyNoAndStatus(tempPolicyNo, RefundStatus.APPROVE, connection);
            if (refundM == null)
                throw new CommonRefundException("Not Found tempPolicyNo or PolicyNo");

            String accountNumber = refundM.getAccountNumber();
            String sensorAccountNumber = sensorAccountNumber(accountNumber);
            
            LOGGER.debug("refundComplete : tempPolicyNo = {}, accountNumber = {}, getPremiumRefund = {}", tempPolicyNo, accountNumber, Double.valueOf(refundM.getPremiumRefund()));

            String smsMessage = String.format("บมจ ไทยประกันชีวิต ได้จ่ายเงินค่าเบี้ยประกันภัย%nจำนวน %,.2f บาท%nที่บัญชี %s เรียบร้อย%nหรือสอบถามเพิ่มเติมที่ตัวแทนของท่าน",
            		Double.valueOf(refundM.getPremiumRefund()), sensorAccountNumber);
            
            return sendSMSAPI(refundM.getMobileNo(), smsMessage);
        } 
        catch (Exception e) {
            
        	if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
            else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }

    private String sensorAccountNumber(String accountNumber) {
        String sensorAccountNumber = "";
        for (int i = 0; i < accountNumber.length(); i++) {
            if (i > 2 && i < accountNumber.length() - 4){
                sensorAccountNumber += "X";
            }
            else{
                sensorAccountNumber += accountNumber.charAt(i);
            }
        }
        return sensorAccountNumber;
    }

    /**
     * 
     * @param mobileNo
     * @param smsMessage
     * @return
     * @throws CommonRefundException
     */
    private String sendSMSAPI(String mobileNo, String smsMessage) throws CommonRefundException {
    	
    	LOGGER.debug("sendSMS : mobileno = {}, message = {}", mobileNo, smsMessage);

        String smsServerURL = PropertiesConfig.getInstance().getSmsServerURL();
        if (!EnvType.PROD.equals(PropertiesConfig.getInstance().getEnvType())) {
        	smsServerURL = smsServerURL.replace("smsserver", "196.1.1.28");
		}
        
        JSONObject requestRecord = new JSONObject();
        requestRecord.put("send_priority", "1");
        requestRecord.put("sender_name", "TLMDAPLUS");
        requestRecord.put("sms_campaign", "TLPROPLUS");
        requestRecord.put("mobile_no", mobileNo);
        requestRecord.put("sms_message", smsMessage);

        JSONArray request = new JSONArray();
        request.put(requestRecord);

        Response response = RestClientUtil.post(request.toString(), smsServerURL);
        return response.readEntity(String.class);
    }
    
    /**
     * REC-15
     * https://docs.google.com/spreadsheets/d/1F80dwVVhhp2aCn6tLxH7Joww-smVwQ9ZFG7LZ_GK3oc/edit#gid=1923364939
     * @param mobile
     * @param tempslipno
     * @return
     * @throws CommonRefundException
     * @throws SQLException
     */
    @GET
    @Path("/verifyMobileAgent/{mobile}/{tempslipno}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataServiceResponseMT<SaleinfomationM> verifyMobileAgent(@PathParam("mobile") String mobile, @PathParam("tempslipno") String tempslipno) throws CommonRefundException, SQLException {
        
    	if (StringUtils.isNull(tempslipno))
            throw new CommonRefundException("tempslipno is null");
		
		if (StringUtils.isNull(mobile))
            throw new CommonRefundException("mobile is null");
		
		LOGGER.debug("verifyMobileAgent : mobile = {}, tempslipno = {}, getEnvType = {}", mobile, tempslipno, PropertiesConfig.getInstance().getEnvType());
		
    	try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
    		LOGGER.debug("connection.isClosed() = {}", connection.isClosed());
    		
    		RefundM refundM = refundDAOImpl.findByTempPaymentSlipNo(connection, tempslipno);
    		if (refundM == null) {
        		LOGGER.debug("refundM is null.");
        		return RestClientUtil.returnResponse(StatusCodeType.E.toString(), "findByTempPaymentSlipNo is null.", null);
			}
    		
    		SaleinfomationM saleinfomationM = new SaleinfomationM();
    		
    		if (!"TLProPlus".equalsIgnoreCase(refundM.getChannel())) {
    			
    			saleinfomationM.setPass(true);
    			return RestClientUtil.returnResponse(StatusCodeType.S.toString(), "", saleinfomationM);
			}
    		
    		List<SaleinfomationM> saleinfomationMs = refundServiceImpl.checkDupMobileno(mobile);
    		if (saleinfomationMs.isEmpty()) {
    			saleinfomationM.setPass(true);
			}
    		else {
    			saleinfomationM.setPass(false);
    			
    			for (SaleinfomationM objSaleinfo : saleinfomationMs) {
    				
    				if (objSaleinfo.getFirstname().equalsIgnoreCase(refundM.getFirstname()) 
    						&& objSaleinfo.getLastname().equalsIgnoreCase(refundM.getLastname())) {
        				
    					saleinfomationM.setPass(true);
    					break;
    				}
				}
    		}
    		
    		return RestClientUtil.returnResponse(StatusCodeType.S.toString(), "", saleinfomationM);
        }
    	catch (Exception e) {
    		LOGGER.error("ERROR = {}", e.getMessage());
    		return RestClientUtil.returnResponse(StatusCodeType.E.toString(), e.getMessage(), null);
		}
    }
    
    /**
     * ข้อมูลจำนวนหลักของเลขบัญชีธนาคาร
     * https://docs.google.com/spreadsheets/d/1F80dwVVhhp2aCn6tLxH7Joww-smVwQ9ZFG7LZ_GK3oc/edit#gid=1194840235
     * @param bankcode
     * @return
     * @throws CommonRefundException
     * @throws SQLException
     */
    @GET
    @Path("/getBankDigitsMST/{bankcode}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public DataServiceResponseMT<int[]> getBankDigitsMST(@PathParam("bankcode") String bankcode) throws CommonRefundException, SQLException {
        
    	if (StringUtils.isNull(bankcode))
            throw new CommonRefundException("bankcode is null");
		
		LOGGER.debug("getBankDigitsMST : bankcode = {}", bankcode);
		
		JSONObject headerData = new JSONObject();
		headerData.put("sessionID", UUID.randomUUID().toString());
		headerData.put("sentDateTime", StringUtils.dateToString(new Date(), SimpleDateFormatUtil.getDateFORMATPOSTGRESQL()));
		
		JSONObject requestRecord = new JSONObject();
		requestRecord.put("bank_code", bankcode);
		
		JSONObject body = new JSONObject();
		body.put("headerData", headerData);
		body.put("requestRecord", requestRecord);
		
		String url = PropertiesConfig.getInstance().getIpPortGateWay10102() + "/BankInfo/1.0/getBankDigitsMST";
		Response response = RestClientUtil.post(body.toString(), url);
		String msg = response.readEntity(String.class);
		if (!StringUtils.isNull(msg)) {
			
			JSONObject object = new JSONObject(msg);
			JSONObject responseStatus = object.getJSONObject("responseStatus");
			
			String status = responseStatus.has("status") ? responseStatus.getString("status") : "";
			if ("S".equalsIgnoreCase(status)) {
				
				int[] digits = null;
				
				JSONObject responseRecord = object.getJSONObject("responseRecord");
				JSONArray bankdigits = responseRecord.getJSONArray("bank_digit");
				if (bankdigits.length() > 0) {
					
					digits = new int[bankdigits.length()];
					
					for (int i = 0; i < bankdigits.length(); i++) {
						digits[i] = bankdigits.getInt(i);
					}
					
					return RestClientUtil.returnResponse(StatusCodeType.S.toString(), "", digits);
				}
			}
			else {
				return RestClientUtil.returnResponse(StatusCodeType.E.toString(), "status = " + status, null);
			}
		}
		
		return RestClientUtil.returnResponse(StatusCodeType.E.toString(), "", null);
    }
    
    @POST
    @Path("/updateMobileNoByTempSlipNo")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public int updateMobileNoByTempSlipNo(String requestM) throws CommonRefundException {
    	
    	if (StringUtils.isNull(requestM)) {
    		throw new CommonRefundException("requestM is null");
		}
    	
    	JSONObject requestJSON = new JSONObject(requestM);
    	String tempSlipNo = requestJSON.getString("tempSlipNo");
    	String mobileNo = requestJSON.getString("mobileNo");
    	
    	if (StringUtils.isNull(tempSlipNo))
            throw new CommonRefundException("tempSlipNo is null");

    	if (StringUtils.isNull(mobileNo))
            throw new CommonRefundException("mobileNo is null");

        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
        	connection.setAutoCommit(true);
        	
        	return refundDAOImpl.updateMobilenoByTempSlipno(connection, tempSlipNo, mobileNo);
        } 
        catch (Exception e) {
        	if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
        	else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }
    
    @POST
    @Path("/updateStatusPendingByTempSlipNo")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public int updateStatusPendingByTempSlipNo(String requestM) throws CommonRefundException {
    	
    	if (StringUtils.isNull(requestM)) {
    		throw new CommonRefundException("requestM is null");
		}
    	
    	JSONObject requestJSON = new JSONObject(requestM);
    	String tempSlipNo = requestJSON.getString("tempSlipNo");
    	
    	if (StringUtils.isNull(tempSlipNo))
            throw new CommonRefundException("tempSlipNo is null");

        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
        	connection.setAutoCommit(true);
        	
        	String sql = "UPDATE common_refund.customers_refund SET status = ?, submit_date = NULL WHERE temp_payment_slip_no = ?";
            try (PreparedStatement ps = connection.prepareStatement(sql)) {
                int i = 1;
                ps.setString(i++, RefundStatus.PENDING.getValue());
                ps.setString(i++, tempSlipNo);

                return ps.executeUpdate();
            }
        } 
        catch (Exception e) {
        	if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
        	else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }
    
    @POST
    @Path("/searchCustomersRefundByTempSlipNo")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public RefundMV2 searchCustomersRefundByTempSlipNo(String requestM) throws CommonRefundException {
    	
    	if (StringUtils.isNull(requestM)) {
    		throw new CommonRefundException("requestM is null");
		}
    	
    	JSONObject requestJSON = new JSONObject(requestM);
    	String tempSlipNo = requestJSON.getString("tempSlipNo");
    	
    	if (StringUtils.isNull(tempSlipNo))
            throw new CommonRefundException("tempSlipNo is null");

        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
        	
        	return refundDAOImpl.findByTempPaymentSlipNoV2(connection, tempSlipNo);
        } 
        catch (Exception e) {
        	if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
        	else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }
    
    @POST
    @Path("/deleteCustomersRefundByTempSlipNo")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public int deleteCustomersRefundByTempSlipNo(String requestM) throws CommonRefundException {
    	
    	if (StringUtils.isNull(requestM)) {
    		throw new CommonRefundException("requestM is null");
		}
    	
    	JSONObject requestJSON = new JSONObject(requestM);
    	String tempSlipNo = requestJSON.getString("tempSlipNo");
    	
    	if (StringUtils.isNull(tempSlipNo))
            throw new CommonRefundException("tempSlipNo is null");

        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
        	
        	return refundDAOImpl.deleteByTempPaymentSlipNo(connection, tempSlipNo);
        } 
        catch (Exception e) {
        	if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
        	else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }
    
    @GET
    @Path("/testSMS/{token}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public String testSMS(@PathParam("token") String token) throws CommonRefundException {
        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {

            RefundM refundM = refundDAOImpl.findBySMSToken(token, connection);
            if (refundM == null) {
            	 return "refundM is null. token = " + token;
			}
            
            sendSMS(refundM);
           
            return "success";
        } 
        catch (Exception e) {
            if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
            else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }
    
    @GET
    @Path("/confirmAccountTest/{smsToken}/{mobile}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public String confirmAccountTest(@PathParam("smsToken") String smsToken, @PathParam("mobile") String mobile) throws CommonRefundException, SQLException {

    	try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
           
            RefundM refundM = refundDAOImpl.findBySMSToken(smsToken, connection);
            String bankname = refundDAOImpl.findBankNameByBankCode(refundM.getBankCode(), connection);
            
            String message = String.format("บริษัทฯ ได้รับเลขที่บัญชีสำหรับโอนเบี้ยประกันภัยคืนเป็นที่เรียบร้อย (กรมธรรม์ชั่วคราวเลขที่ %s, ชื่อ-นามสกุล คุณ%s %s, ธนาคาร%s, เลขที่บัญชี %s) หากมีข้อสงสัยกรุณาสอบถามตัวแทนของท่าน", 
            		refundM.getTempPolicyNo(), refundM.getFirstname(), refundM.getLastname(), bankname.replace("ธ.", ""), refundM.getAccountNumber());
            
            return sendSMSAPI(mobile, message);
    	}
    }
    
    @GET
    @Path("/getEnv")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public String getEnv() throws CommonRefundException, SQLException {
    	return PropertiesConfig.getInstance().getEnvType().toString();
    }
    
    @GET
    @Path("/saveLogTest/{smsToken}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public RefundM saveLogTest(@PathParam("smsToken") String smsToken) throws CommonRefundException {
    	
        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
        	connection.getAutoCommit();

            RefundM refundM = new RefundM();
            smsToken = smsToken.replace(" ", "+");
            refundM = refundDAOImpl.findBySMSToken(smsToken, connection);
            if (refundM != null) {
                refundM.setBankName(refundDAOImpl.findBankNameByBankCode(refundM.getBankCode(), connection));
                logRefundDetail.save(connection, refundM.getTempPaymentSlipNo(), refundM.getApplicationId(), "Confirm refund", "url", "req", "res", "REFUND");
                return refundM;
			}
            else {
            	 return refundM;
            }
        } 
        catch (Exception e) {
            if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
            else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }
    
    @GET
    @Path("/searchLogRefund/{searchValue}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<LogDetailM> searchLogRefund(@PathParam("searchValue") String searchValue) throws CommonRefundException {
    	
        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
        	return logRefundDetail.search(connection, searchValue);
        } 
        catch (Exception e) {
            if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
            else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }
    
    @GET
    @Path("/countBySMSToken/{smsToken}")
    @Produces({MediaType.APPLICATION_JSON})
    public int countBySMSToken(@PathParam("smsToken") String smsToken) throws CommonRefundException {
    	
        try (Connection connection = DataBaseConnector.getInstance().getConnectionDB()) {
        	
        	int total = 0; 
			try (Statement statement = connection.createStatement()){
				try (ResultSet rs = statement.executeQuery(String.format("select count(*) from common_refund.customers_refund where sms_token = '%s'", smsToken))) {
					while (rs.next()) {
						total = rs.getInt(1);
					}
				}
			}
        	
        	return total;
        } 
        catch (Exception e) {
            if (StringUtils.isNull(e.getMessage())) {
                throw new CommonRefundException(e);
            } 
            else {
                throw new CommonRefundException(e.getMessage());
            }
        }
    }

}
