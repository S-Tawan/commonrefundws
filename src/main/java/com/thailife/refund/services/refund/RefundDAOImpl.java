package com.thailife.refund.services.refund;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.json.JSONObject;

import com.thailife.refund.constant.RefundStatus;
import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.models.dao.RefundM;
import com.thailife.refund.models.dao.RefundMV2;
import com.thailife.refund.util.PropertiesConfig;
import com.thailife.refund.util.RestClientUtil;
import com.thailife.refund.util.StringUtils;

public class RefundDAOImpl {

    public void insert(RefundM refundM, Connection connection) throws SQLException {
        String sql = " INSERT INTO common_refund.customers_refund (refund_id," +
                " application_id," +
                " token," +
                " prename," +
                " firstname," +
                " lastname," +
                " identify_id," +
                " agent_id," +
                " policy_no," +
                " temp_policy_no," +
                " application_status," +
                " payment_type," +
                " payment_type_desc," +
                " temp_payment_slip_no," +
                " mobile_no," +
                " credit_card_no," +
                " channel," +
                " premium_refund ," +
                " create_date," +
                " update_date, generate_date) " +
                "VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, DEFAULT, DEFAULT, DEFAULT)";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            int i = 1;
            ps.setInt(i++, Integer.parseInt(refundM.getApplicationId()));
            ps.setString(i++, refundM.getToken());
            ps.setString(i++, refundM.getPrename());
            ps.setString(i++, refundM.getFirstname());
            ps.setString(i++, refundM.getLastname());
            ps.setString(i++, refundM.getIdentifyId());
            ps.setString(i++, refundM.getAgentId());
            ps.setString(i++, refundM.getPolicyNo());
            ps.setString(i++, refundM.getTempPolicyNo());
            ps.setString(i++, refundM.getApplicationStatus());
            ps.setString(i++, refundM.getPaymentType());
            ps.setString(i++, refundM.getPaymentTypeDesc());
            ps.setString(i++, refundM.getTempPaymentSlipNo());
            ps.setString(i++, refundM.getMobileNo());
            ps.setString(i++, refundM.getCreditCardNo());
            ps.setString(i++, refundM.getChannel());
            ps.setString(i++, refundM.getPremiumRefund());
            ps.executeUpdate();
        }
    }

    public void updateByTempPaymentSlipNo(RefundM refundM, String tempPaymentSlipNo, Connection connection) throws SQLException {
        String sql = "update common_refund.customers_refund " +
                "set application_id = ?, " +
                " token = ?, " +
                " prename = ?, " +
                " firstname = ?, " +
                " lastname = ?, " +
                " identify_id = ?, " +
                " agent_id = ?, " +
                " policy_no = ?, " +
                " temp_policy_no = ?, " +
                " application_status = ?," +
                " payment_type = ?, " +
                " payment_type_desc = ?, " +
                " temp_payment_slip_no = ?, " +
                " credit_card_no = ?, " +
                " channel = ?, " +
                " premium_refund = ? ," +
                " update_date = ?, " +
                " generate_date = ? " +
                "where temp_payment_slip_no = ?  and status = ? ";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            int i = 1;
            ps.setInt(i++, Integer.parseInt(refundM.getApplicationId()));
            ps.setString(i++, refundM.getToken());
            ps.setString(i++, refundM.getPrename());
            ps.setString(i++, refundM.getFirstname());
            ps.setString(i++, refundM.getLastname());
            ps.setString(i++, refundM.getIdentifyId());
            ps.setString(i++, refundM.getAgentId());
            ps.setString(i++, refundM.getPolicyNo());
            ps.setString(i++, refundM.getTempPolicyNo());
            ps.setString(i++, refundM.getApplicationStatus());
            ps.setString(i++, refundM.getPaymentType());
            ps.setString(i++, refundM.getPaymentTypeDesc());
            ps.setString(i++, refundM.getTempPaymentSlipNo());
//            ps.setString(i++, refundM.getMobileNo());
            ps.setString(i++, refundM.getCreditCardNo());
            ps.setString(i++, refundM.getChannel());
            ps.setString(i++, refundM.getPremiumRefund());
            ps.setTimestamp(i++, Timestamp.valueOf(LocalDateTime.now()));
            ps.setTimestamp(i++, Timestamp.valueOf(LocalDateTime.now()));

            ps.setString(i++, tempPaymentSlipNo);
            ps.setString(i++, RefundStatus.PENDING.getValue());

            ps.executeUpdate();
        }
    }

    public int updateDetailAccount(RefundM refundM, Connection connection) throws SQLException, CommonRefundException {
        String sql = "update common_refund.customers_refund " +
                "set refund_type    = ?, " +
                "    full_name      = ?, " +
                "    bank_code           = ?, " +
                "    account_number = ?, " +
                "    submit_date = ?, " +
                "    sms_token = ? " +
                "where token = ? and status = ? ";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            int i = 1;

            ps.setString(i++, refundM.getRefundType());
            ps.setString(i++, refundM.getFullName());
            ps.setString(i++, refundM.getBankCode());
            ps.setString(i++, refundM.getAccountNumber());
            ps.setTimestamp(i++, Timestamp.valueOf(LocalDateTime.now()));
            ps.setString(i++, refundM.getSmsToken());

            ps.setString(i++, refundM.getToken());
            ps.setString(i++, RefundStatus.PENDING.getValue());

            return ps.executeUpdate();

        } catch (Exception e) {
            throw new CommonRefundException(e);
        }

    }


    /**
     * 
     * @param status
     * @param smsToken
     * @param connection
     * @return
     * @throws SQLException
     * @throws CommonRefundException
     */
    public int updateStatusBySmsToken(String status, String smsToken, Connection connection) throws SQLException, CommonRefundException {
        String sql = "update common_refund.customers_refund " +
                "set status  = ? " +
                "where sms_token = ? and status = ? ";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            int i = 1;
            ps.setString(i++, status);
            ps.setString(i++, smsToken);
            ps.setString(i++, RefundStatus.PENDING.getValue());

            return ps.executeUpdate();

        } catch (Exception e) {
            throw new CommonRefundException(e);
        }
    }


    public String findBankNameByBankCode(String bankCode, Connection connection) throws SQLException {
        String sql = "SELECT bankname FROM common_refund.bank_detail  where bankcode = ? ";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, bankCode);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getString("bankname");
                }
            }
        }
        return null;
    }

    public RefundM findByToken(String token, Connection connection) throws SQLException {

        String sql = "SELECT * FROM common_refund.customers_refund  where token = ? AND status = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, token);
            ps.setString(2, RefundStatus.PENDING.getValue());
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return new RefundM(resultSet);
                }
            }
        }
        return null;
    }

    public RefundM findBySMSToken(String token, Connection connection) throws SQLException {

        String sql = "SELECT * FROM common_refund.customers_refund where sms_token = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, token);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return new RefundM(resultSet);
                }
            }
        }
        return null;
    }

    public RefundM findByApplicationId(String appId, Connection connection) throws SQLException {
        String sql = "SELECT * FROM common_refund.customers_refund  where application_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, appId);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return new RefundM(resultSet);
                }
            }
        }
        return null;
    }

    public RefundM findByTempPaymentSlipNoAndStatus(String tempPaymentSlipNo,RefundStatus refundStatus,  Connection connection) throws SQLException {
        String sql = "SELECT * FROM common_refund.customers_refund  where temp_payment_slip_no = ? and status = ? order by submit_date DESC";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, tempPaymentSlipNo);
            ps.setString(2, refundStatus.name());
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return new RefundM(resultSet);
                }
            }
        }
        return null;
    }
    
    /**
     * 
     * @param tempPaymentSlipNo
     * @param connection
     * @return
     * @throws SQLException
     */
    public RefundM findByTempPaymentSlipNo(Connection connection, String tempPaymentSlipNo) throws SQLException {
        
    	String sql = "SELECT * FROM common_refund.customers_refund  where temp_payment_slip_no = ? order by submit_date DESC";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, tempPaymentSlipNo);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return new RefundM(resultSet);
                }
            }
        }
        return null;
    }
    
    /**
     * 
     * @param connection
     * @param tempPaymentSlipNo
     * @return
     * @throws SQLException
     */
    public int deleteByTempPaymentSlipNo(Connection connection, String tempPaymentSlipNo) throws SQLException {
        
    	String sql = "DELETE FROM common_refund.customers_refund WHERE temp_payment_slip_no = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, tempPaymentSlipNo);
            return ps.executeUpdate();
        }
    }
    
    public RefundM findByTempPolicyNoOrPolicyNoAndStatus(String tempPolicyNo, RefundStatus refundStatus, Connection connection) throws SQLException {
        String sql = "SELECT * FROM common_refund.customers_refund  where ( temp_policy_no = ? or policy_no = ? ) and status = ? order by submit_date DESC";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, tempPolicyNo);
            ps.setString(2, tempPolicyNo);
            ps.setString(3, refundStatus.name());
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return new RefundM(resultSet);
                }
            }
        }
        return null;
    }

    /**
     * @param applicationIdList
     * @param connection
     * @return
     * @throws SQLException
     */
    public Map<String, Boolean> checkDisableBtnByApplicationId(List<String> applicationIdList,int minute, Connection connection) throws SQLException {

        Map<String, Boolean> booleans = new HashMap<>();
        String value = String.format("('%s')", String.join("'),('", applicationIdList));
        String sql = String.format("with app (app_id) as ( " +
                "    values %s " +
                ") " +
                "select app.app_id, " +
                "       bool_and(c.submit_date + (%s * interval '1 minute') > now()) AS result " +
                "from app " +
                "         inner join common_refund.customers_refund c on app.app_id = c.application_id and c.status = 'PENDING' " +
                "group by app.app_id;", value, minute);

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    booleans.put(resultSet.getString("app_id"), resultSet.getBoolean("result"));
                }
            }
        }

        return booleans;
    }

    /**
     * 
     * @param paymentslipno
     * @param confirm
     * @param msg
     * @param channel
     * @return
     * @throws CommonRefundException
     */
    public boolean updateApplicationStatus(String paymentslipno, String confirm, String msg, String channel) throws CommonRefundException {

    	
    	 String url = PropertiesConfig.getInstance().getIpPortTLPro() + "/TLPromptBackendWs/rest/e2e/updateCommonRefundStatusService";
    	if (!"TLProPlus".equalsIgnoreCase(channel)) {
    		url = PropertiesConfig.getInstance().getIpPortTLLifePlus() + "/CIMBBackendWs/rest/e2e/updateCommonRefundStatusService";
		}

        JSONObject object = new JSONObject();
        object.put("paymentslipno", paymentslipno);
        object.put("resmsg", msg);
        object.put("applicationstatus", "Y".equalsIgnoreCase(confirm) ? "38" : "39");

        Response response = RestClientUtil.<String>post(object.toString(), url);
        String res = response.readEntity(String.class);
        if (!StringUtils.isNull(res)) {
            JSONObject resObj = new JSONObject(res);
            if ("Y".equalsIgnoreCase(resObj.get("returnCode").toString())) {
                return true;
            }
        }

        return false;
    }

	public int updateMobilenoBytoken(Connection connection, String token, String mobile) throws CommonRefundException {

        String sql = "UPDATE common_refund.customers_refund SET mobile_no = ? WHERE token = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            int i = 1;
            ps.setString(i++, mobile);
            ps.setString(i++, token);

            return ps.executeUpdate();

        } catch (Exception e) {
            throw new CommonRefundException(e);
        }
	}
	
	/**
	 * 
	 * @param connection
	 * @param tempSlipno
	 * @param mobile
	 * @return
	 * @throws CommonRefundException
	 */
	public int updateMobilenoByTempSlipno(Connection connection, String tempSlipno, String mobile) throws CommonRefundException {

        String sql = "UPDATE common_refund.customers_refund SET mobile_no = ?, update_date = NOW() WHERE temp_payment_slip_no = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            int i = 1;
            ps.setString(i++, mobile);
            ps.setString(i++, tempSlipno);

            return ps.executeUpdate();

        } catch (Exception e) {
            throw new CommonRefundException(e);
        }
	}

	public RefundMV2 findByTempPaymentSlipNoV2(Connection connection, String value) throws SQLException {
    	
    	RefundMV2 refundMV2 = new RefundMV2();
    	String sql = "SELECT * FROM common_refund.customers_refund WHERE temp_payment_slip_no = ? ORDER BY submit_date DESC";
    	
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, value);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                	
                    int refundId = resultSet.getInt("refund_id");
                    String applicationId = resultSet.getString("application_id");
                    String token = resultSet.getString("token");
                    String smsToken = resultSet.getString("sms_token");
                    String prename = resultSet.getString("prename");
                    String firstname = resultSet.getString("firstname");
                    String lastname = resultSet.getString("lastname");
                    String identifyId = resultSet.getString("identify_id");
                    String agentId = resultSet.getString("agent_id");
                    String policyNo = resultSet.getString("policy_no");
                    String tempPolicyNo = resultSet.getString("temp_policy_no");
                    String paymentType = resultSet.getString("payment_type");
                    String paymentTypeDesc = resultSet.getString("payment_type_desc");
                    String tempPaymentSlipNo = resultSet.getString("temp_payment_slip_no");
                    String mobileNo = resultSet.getString("mobile_no");
                    String channel = resultSet.getString("channel");
                    String status = resultSet.getString("status");
                    String applicationStatus = resultSet.getString("application_status");
                    String refundType = resultSet.getString("refund_type");
                    String fullName = resultSet.getString("full_name");
                    String bankCode = resultSet.getString("bank_code");
                    String accountNumber = resultSet.getString("account_number");
                    Timestamp generateDate = resultSet.getTimestamp("generate_date");
                    Timestamp submitDate = resultSet.getTimestamp("submit_date");
                    String premiumRefund = resultSet.getString("premium_refund");
                    Timestamp createDate = resultSet.getTimestamp("create_date");
                    Timestamp updateDate = resultSet.getTimestamp("update_date");
                	
                    refundMV2.setAccountNumber(accountNumber);
                    refundMV2.setAgentId(agentId);
                    refundMV2.setApplicationId(applicationId);
                    refundMV2.setApplicationStatus(applicationStatus);
                    refundMV2.setBankCode(bankCode);
                    refundMV2.setBankName("");
                    refundMV2.setChannel(channel);
                    refundMV2.setCreateDate(createDate.toString());
                    refundMV2.setCreditCardNo("");
                    refundMV2.setFirstname(firstname);
                    refundMV2.setFullName(fullName);
                    refundMV2.setGenerateDate(generateDate.toString());
                    refundMV2.setIdentifyId(identifyId);
                    refundMV2.setLastname(lastname);
                    refundMV2.setMobileNo(mobileNo);
                    refundMV2.setPaymentType(paymentType);
                    refundMV2.setPaymentTypeDesc(paymentTypeDesc);
                    refundMV2.setPolicyNo(policyNo);
                    refundMV2.setPremiumRefund(premiumRefund);
                    refundMV2.setPrename(prename);
                    refundMV2.setRefundId(Integer.toString(refundId));
                    refundMV2.setRefundType(refundType);
                    refundMV2.setSmsToken(smsToken);
                    refundMV2.setStatus(status);
                    refundMV2.setSubmitDate(submitDate.toString());
                    refundMV2.setTempPaymentSlipNo(tempPaymentSlipNo);
                    refundMV2.setTempPolicyNo(tempPolicyNo);
                    refundMV2.setToken(token);
                    refundMV2.setUpdateDate(updateDate.toString());
                }
            }
        }
        
        return refundMV2;
    }

}
