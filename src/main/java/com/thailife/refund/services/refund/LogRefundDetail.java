package com.thailife.refund.services.refund;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thailife.refund.models.dao.LogDetailM;
import com.thailife.refund.util.SimpleDateFormatUtil;
import com.thailife.refund.util.StringUtils;

public class LogRefundDetail {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(LogRefundDetail.class);

	/**
	 * 
	 * @param connection
	 * @param tempPaymentSlipNo
	 * @param applicationId
	 * @param detail
	 * @param url
	 * @param request
	 * @param response
	 * @param typeslog
	 * @throws SQLException 
	 */
	public void save(Connection connection, String tempPaymentSlipNo, String applicationId, String detail,
			String url, String request, String response, String typeslog) throws SQLException {
		
		try {
			String sql = "INSERT INTO common_refund.logdetail(temprpno, applicationid, createdate, detail, url, request, response, typeslog) "
					+ "VALUES(?, ?, NOW(), ?, ?, ?, ?, ?)";
			
			try (PreparedStatement ps = connection.prepareStatement(sql)) {
	            int i = 1;
	            ps.setString(i++, tempPaymentSlipNo);
	            ps.setString(i++, applicationId);
	            ps.setString(i++, detail);
	            ps.setString(i++, url);
	            ps.setString(i++, request);
	            ps.setString(i++, response);
	            ps.setString(i++, typeslog);
	            ps.executeUpdate();
	        }
		} 
		catch (Exception e) {
			LOGGER.error("Exception msg = {}, {}", e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @param connection
	 * @param searchValue
	 * @return
	 * @throws SQLException
	 */
	public List<LogDetailM> search(Connection connection, String searchValue) throws SQLException {
		
		String sql = "select * from common_refund.logdetail where temprpno = ? order by createdate desc";
		 
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, searchValue);
            try (ResultSet resultSet = ps.executeQuery()) {
            	
            	List<LogDetailM> logDetailMs = new ArrayList<>();
            	
            	while (resultSet.next()) {
                	
                	 int seq = resultSet.getInt("seq");
                	 String temprpno = resultSet.getString("temprpno");
                	 String applicationid = resultSet.getString("applicationid");
                	 String detail = resultSet.getString("detail");
                	 String url = resultSet.getString("url");
                	 String request = resultSet.getString("request");
                	 String response = resultSet.getString("response");
                	 String typeslog = resultSet.getString("typeslog");
                	 String createdateStr = "";
                	 Timestamp createdate = resultSet.getTimestamp("createdate");
                	 if (createdate != null) {
                		 Date date = new Date(createdate.getTime());
                    	 createdateStr = StringUtils.dateToString(date, SimpleDateFormatUtil.getDateFORMATPOSTGRESQL());
                	 }
                	 
                	 
                	 LogDetailM logDetailM = new LogDetailM();
                	 logDetailM.setSeq(Integer.toString(seq));
                	 logDetailM.setTemprpno(temprpno);
                	 logDetailM.setApplicationid(applicationid);
                	 logDetailM.setDetail(detail);
                	 logDetailM.setUrl(url);
                	 logDetailM.setRequest(request);
                	 logDetailM.setResponse(response);
                	 logDetailM.setTypeslog(typeslog);
                	 logDetailM.setCreatedate(createdateStr);
                	 
                	 logDetailMs.add(logDetailM);
                }
                
                return logDetailMs;
            }
        }
	}

}
