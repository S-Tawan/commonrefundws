package com.thailife.refund.services;

import com.thailife.refund.services.otp.OTPGerarateService;
import com.thailife.refund.services.refund.RefundService;
import com.thailife.refund.services.underwrite.UnderwriteRefundService;

import com.thailife.tlafter.paymentcallback.PaymentCallbackService;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class WebConfig extends Application {
	
    private Set<Object> singletons = new HashSet<>();

    public WebConfig() {

        singletons.add(new RefundService());
        singletons.add(new UnderwriteRefundService());
        singletons.add(new LoadPropertiesConfigService());
        singletons.add(new PaymentCallbackService());
        singletons.add(new OTPGerarateService());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}