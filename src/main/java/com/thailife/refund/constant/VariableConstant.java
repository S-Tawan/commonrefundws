package com.thailife.refund.constant;

public enum VariableConstant {
	
	SESSION_ID("sessionID"),
	
	GENDER_ALL("A"),
	
	MSG_UTILITYCLASS("Utility class."),
	
	NUMBER_FORMAT("00000000"),
	
	DELETEFLAG("C"),
	
	INCREASEONE("1"),
	
	NUMBER_FORMAT_9("000000000"),
	
	REQUESTURL("requestURL"),
	
	UUID("uuID"),
	
	IP_ADDRESS("ipAddress"),
	
	HOST("host"), 
	
	AGENT_ID("agentid"),
	
	REAL_IP("realIP");
	
	private String value;
	
	private VariableConstant(String key) {
		this.value = key;
	}
	
	public String getValue() {
		return value;
	}
}
