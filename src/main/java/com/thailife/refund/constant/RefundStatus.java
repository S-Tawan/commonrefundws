package com.thailife.refund.constant;

public enum RefundStatus {

	PENDING("PENDING"),

	APPROVE("Y"),

	REFUSE("N");

	private String value;

	private RefundStatus(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
