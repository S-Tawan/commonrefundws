package com.thailife.refund.constant;

public enum BankName {

	PENDING("PENDING"),

	CONFIRM("CONFIRM"),

	REJECT("REJECT");

	private String value;

	private BankName(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
