package com.thailife.refund.constant;

public enum EnvType {
	
    LOCALHOST, DEV, SIT, UAT, PREPROD, PROD
    
}