package com.thailife.refund.constant;

public enum RefundType {

	PENDING("PENDING"),

	CONFIRM("CONFIRM"),

	REJECT("REJECT");

	private String value;

	private RefundType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
