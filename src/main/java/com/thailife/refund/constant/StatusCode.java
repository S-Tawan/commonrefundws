package com.thailife.refund.constant;

public enum StatusCode {

	SUCCESS("0"),
	
	DATA_NOTFOUND("1"),
	
	ERROR("2");
	
	private String value;
	
	private StatusCode(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
