package com.thailife.refund.util;

import java.util.Collection;

import com.thailife.refund.constant.VariableConstant;
import com.thailife.refund.exception.CommonRefundException;

public class CollectionUtils {
	
	private CollectionUtils() throws CommonRefundException {
		throw new CommonRefundException(VariableConstant.MSG_UTILITYCLASS.getValue());
	}
	
	public static boolean isEmpty(Collection<?> collection) {
	    return (collection == null || collection.isEmpty());
	}
	
}
