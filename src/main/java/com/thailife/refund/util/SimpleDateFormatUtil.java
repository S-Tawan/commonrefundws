package com.thailife.refund.util;

import java.text.SimpleDateFormat;

import com.thailife.refund.constant.VariableConstant;
import com.thailife.refund.exception.CommonRefundException;


public class SimpleDateFormatUtil {
	
	private static final String FORMAT_DDMMYYYY_T_HHMMSS = "dd-MM-yyyy'T'hh:mm:ss.SSS";
	
	private static final String FORMAT_DDMMYYYY = "dd/MM/yyyy";
	
	private static final String FORMAT_DDMMYYYY_HHMMSS = "dd/MM/yyyy HH:MM:SS";

	private static final String FORMAT_YYYYMMDD = "yyyy-MM-dd";
	
	public static final String FORMAT_YYYYMMDD_TH = "yyyyMMdd";
	
	private static final String FORMAT_POSTGRESQL = "yyyy-MM-dd HH:mm:ss";// .SSS
	
	private static final String FORMAT_POSTGRESQL_T = "yyyy-MM-dd'T'HH:mm:ss";// .SSS
	
	private SimpleDateFormatUtil() throws CommonRefundException {
		throw new CommonRefundException(VariableConstant.MSG_UTILITYCLASS.getValue());
	}
	
	public static SimpleDateFormat getDateFormatHeader() {
		return new SimpleDateFormat(FORMAT_DDMMYYYY_HHMMSS);
	}
	
	public static SimpleDateFormat getDateFormatDDMMYYY() {
		return new SimpleDateFormat(FORMAT_DDMMYYYY);
	}
	
	public static SimpleDateFormat getDateFormatDDMMYYYTHHMMSS() {
		return new SimpleDateFormat(FORMAT_DDMMYYYY_T_HHMMSS);
	}
	
	public static SimpleDateFormat getDateFormatYYYYMMDD() {
		return new SimpleDateFormat(FORMAT_YYYYMMDD);
	}
	
	public static SimpleDateFormat getDateFORMATPOSTGRESQL() {
		return new SimpleDateFormat(FORMAT_POSTGRESQL);
	}
	
	public static SimpleDateFormat getDateFORMATT() {
		return new SimpleDateFormat(FORMAT_POSTGRESQL_T);
	}
	
	public static SimpleDateFormat getDateFORMATYYYYMMDDTH() {
		return new SimpleDateFormat(FORMAT_YYYYMMDD_TH);
	}
	
}
