package com.thailife.refund.util;

import java.io.Serializable;
import java.util.Date;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thailife.refund.constant.StatusCode;
import com.thailife.refund.constant.StatusCodeType;
import com.thailife.refund.constant.VariableConstant;
import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.models.BodyResponseM;
import com.thailife.refund.models.DataServiceResponseMT;
import com.thailife.refund.models.HeaderData;
import com.thailife.refund.models.RequestParameterM;
import com.thailife.refund.models.ResponseStatus;

public class RestClientUtil {
	
	private static final String BEARER = "Bearer ";
	
	private static final String ENV_LOCALHOST = "localhost";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestClientUtil.class);
	
	private RestClientUtil() {
		throw new IllegalStateException(VariableConstant.MSG_UTILITYCLASS.getValue());
	}

	/**
	 * 
	 * @param request
	 * @param url
	 * @return
	 * @throws CommonRefundException
	 */
	public static <X extends Serializable> Response post(X request, String url) throws CommonRefundException {
		
		LOGGER.debug("post : URL = {}, request = {}", url, request);
		
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget resteasyWebTarget = client.target(url);
		
		Builder builder = resteasyWebTarget.request();
		Response response = builder.post(Entity.entity(request, MediaType.APPLICATION_JSON));
		
		if (response.getStatus() != 200 && response.getStatus() != 500)
			throw new CommonRefundException(response.getStatusInfo().getReasonPhrase() + " >> URL = " + url);
		
		return response;
	}

	public static <X extends Serializable, Y extends Serializable> DataServiceResponseMT<Y> returnResponse(RequestParameterM<X> request, String statusCodeType, String errormsg, Y response) {
		
		BodyResponseM<Y> body = new BodyResponseM<>();
    	ResponseStatus status = new ResponseStatus();
    	
    	if (StatusCodeType.S.toString().equalsIgnoreCase(statusCodeType)) {
    		
    		if (response != null)
    			body.setData(response);
        	
        	status.setErrorCode(StatusCode.SUCCESS.getValue());
        	status.setErrorMessage(StatusCodeType.S.getValue());
        	status.setStatus(StatusCodeType.S.toString());
		}
    	else {
    	
        	status.setErrorCode(StatusCode.ERROR.getValue());
        	status.setErrorMessage(errormsg);
        	status.setStatus(StatusCodeType.E.toString());
    	}
    	
		HeaderData headers = request.getHeaders();
//		headers.setResponseDateTime(StringUtils.dateToString(new Date()));
    	
    	DataServiceResponseMT<Y> responseMT = new DataServiceResponseMT<>();
		responseMT.setBody(body);
		responseMT.setHeaders(headers);
		responseMT.setStatus(status);
    	
    	return responseMT;
	}
	
	

	public static <Y extends Serializable> DataServiceResponseMT<Y> returnResponse(String statusCodeType, String errormsg, Y response) {
		
		BodyResponseM<Y> body = new BodyResponseM<>();
    	ResponseStatus status = new ResponseStatus();
    	
    	if (StatusCodeType.S.toString().equalsIgnoreCase(statusCodeType)) {
    		
    		if (response != null)
    			body.setData(response);
        	
        	status.setErrorCode(StatusCode.SUCCESS.getValue());
        	status.setErrorMessage(StatusCodeType.S.getValue());
        	status.setStatus(StatusCodeType.S.toString());
		}
    	else {
    	
        	status.setErrorCode(StatusCode.ERROR.getValue());
        	status.setErrorMessage(errormsg);
        	status.setStatus(StatusCodeType.E.toString());
    	}
    	
		HeaderData headers = new HeaderData();
		headers.setResponseDateTime(StringUtils.dateToString(new Date(), SimpleDateFormatUtil.getDateFORMATPOSTGRESQL()));
    	
    	DataServiceResponseMT<Y> responseMT = new DataServiceResponseMT<>();
		responseMT.setBody(body);
		responseMT.setHeaders(headers);
		responseMT.setStatus(status);
    	
    	return responseMT;
	}
}
