package com.thailife.refund.util;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thailife.refund.constant.EnvType;

public class PropertiesConfig {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesConfig.class);

	private static PropertiesConfig instance;
	
	private EnvType envType = EnvType.PROD;
	
	private String ipPortTLPro;
	
	private String ipPortTLLifePlus;
	
	private String smsServerURL;
	
	private String ipPort10102;
	
	private String ipPortGateWay10102;

	private String commonRefundWebview;
	
	private String dbHost = "";
	
	private String dbUsername = "";
	
	private String dbPassword = "";
	
	private PropertiesConfig() {
		
		setEnvironmentType();
		
		switch (envType) {
		
		case LOCALHOST:
			
			ipPortTLLifePlus = "http://uat-tlproplus-tws.thailife.com";
			ipPortTLPro = "http://uat-tlproplus-tws.thailife.com";
			smsServerURL = "http://smsserver:8080/SmsClickNext/rest/sms/received/list";
			ipPort10102 = "http://10.102.65.20:8080";
			ipPortGateWay10102 = "http://10.102.65.114:8280";
			commonRefundWebview = "https://uat-channel.thailife.com/tlprorefund/refund/review/";
			
			dbHost = "uat-odsdb.thailife.com";
			dbUsername = "customer.app";
			dbPassword = "ADe8TwkYTuyB5G";
		
			break;
			
		case DEV:
			
			ipPortTLLifePlus = "http://dev-tlproplus-tws.thailife.com";
			ipPortTLPro = "http://dev-tlproplus-tws.thailife.com";
			smsServerURL = "http://196.1.1.28:8080/SmsClickNext/rest/sms/received/list";
			ipPort10102 = "http://10.102.60.20:8080";
			ipPortGateWay10102 = "http://10.102.60.112:8280";
			commonRefundWebview = "https://dev-channel.thailife.com/tlprorefund/refund/review/";
			
			dbHost = "dev-odsdb.thailife.com";
			dbUsername = "customer.app";
			dbPassword = "Y2ZkNzk5M2MaYW";
			
			break;
			
		case SIT:
			
			ipPortTLLifePlus = "http://sit-tlproplus-tws.thailife.com";
			ipPortTLPro = "http://sit-tlproplus-tws.thailife.com";
			smsServerURL = "http://smsserver:8080/SmsClickNext/rest/sms/received/list";
			ipPort10102 = "http://10.102.63.20:8080";
			ipPortGateWay10102 = "http://10.102.63.112:8280";
			commonRefundWebview = "https://sit-channel.thailife.com/tlprorefund/refund/review/";
			
			dbHost = "sit-odsdb.thailife.com";
			dbUsername = "customer.app";
			dbPassword = "P7HkNzk5M7MaYW";
			
			break;
			
		case UAT:
			
			ipPortTLLifePlus = "http://uat-tlproplus-tws.thailife.com";
			ipPortTLPro = "http://uat-tlproplus-tws.thailife.com";
			smsServerURL = "http://smsserver:8080/SmsClickNext/rest/sms/received/list";
			ipPort10102 = "http://10.102.65.20:8080";
			ipPortGateWay10102 = "http://10.102.65.114:8280";
			commonRefundWebview = "https://uat-channel.thailife.com/tlprorefund/refund/review/";
			
			dbHost = "uat-odsdb.thailife.com";
			dbUsername = "customer.app";
			dbPassword = "ADe8TwkYTuyB5G";
			
			break;
			
		case PREPROD:
			
			ipPortTLLifePlus = "http://pre-partner-tws.thailife.com";
			ipPortTLPro = "http://pre-tlproplus-tws.thailife.com";
			smsServerURL = "http://smsserver:8080/SmsClickNext/rest/sms/received/list";
			ipPort10102 = "http://10.102.45.43:8080";
			ipPortGateWay10102 = "http://10.102.45.33:8280";
			commonRefundWebview = "https://pre-channel.thailife.com/tlprorefund/refund/review/";
			
			break;
			
		case PROD:
			
			ipPortTLLifePlus = "http://partner-tws.thailife.com";
			ipPortTLPro = "http://tlproplus-tws.thailife.com";
			smsServerURL = "http://smsserver:8080/SmsClickNext/rest/sms/received/list";
			ipPort10102 = "http://10.102.44.43:8080";
			ipPortGateWay10102 = "http://10.102.44.33:8280";
			commonRefundWebview = "https://channel.thailife.com/tlprorefund/refund/review/";
			
			break;
			
		default:
			
			ipPortTLLifePlus = "http://partner-tws.thailife.com";
			ipPortTLPro = "http://tlproplus-tws.thailife.com";
			smsServerURL = "http://smsserver:8080/SmsClickNext/rest/sms/received/list";
			ipPort10102 = "http://10.102.44.43:8080";
			ipPortGateWay10102 = "http://10.102.44.33:8280";
			commonRefundWebview = "https://channel.thailife.com/tlprorefund/refund/review/";
			
			break;
		}
	}

	private void setEnvironmentType() {
		
		HashMap<String, EnvType> envMap = new HashMap<>();
		envMap.put("10.102.60.20", EnvType.DEV);
		envMap.put("10.102.63.20", EnvType.SIT);
		
		envMap.put("10.102.65.20", EnvType.UAT);
		envMap.put("10.102.69.24", EnvType.UAT);
		envMap.put("10.102.69.25", EnvType.UAT);
		envMap.put("10.102.69.44", EnvType.UAT);
		envMap.put("10.102.69.45", EnvType.UAT);
		envMap.put("10.102.77.30", EnvType.UAT);
		envMap.put("10.102.65.71", EnvType.UAT);
		
		envMap.put("10.102.45.43", EnvType.PREPROD);
		envMap.put("10.102.43.36", EnvType.PREPROD);
		envMap.put("10.102.43.37", EnvType.PREPROD);
		envMap.put("10.102.43.34", EnvType.PREPROD);
		envMap.put("10.102.43.35", EnvType.PREPROD);
		envMap.put("10.102.75.25", EnvType.PREPROD);
		envMap.put("10.102.75.26", EnvType.PREPROD);
		
		envMap.put("10.102.44.43", EnvType.PROD);
		envMap.put("10.102.49.38", EnvType.PROD);
		envMap.put("10.102.49.39", EnvType.PROD);
		envMap.put("10.102.49.36", EnvType.PROD);
		envMap.put("10.102.49.37", EnvType.PROD);
		envMap.put("10.102.74.27", EnvType.PROD);
		envMap.put("10.102.74.28", EnvType.PROD);
		
		try (final DatagramSocket socket = new DatagramSocket()) {
			socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
			String ip = socket.getLocalAddress().getHostAddress();
			
			envType = envMap.get(ip);
			if (envType == null) {
				envType = EnvType.PROD;
			}
		}
		catch (Exception e) {
			LOGGER.debug(e.getMessage());
		}
	}


	public static PropertiesConfig getInstance() {
		
		if (instance == null)
			instance = new PropertiesConfig();

		return instance;
	}

	public String getIpPortTLLifePlus() {
		return ipPortTLLifePlus;
	}

	public void setIpPortTLLifePlus(String ipPortTLLifePlus) {
		this.ipPortTLLifePlus = ipPortTLLifePlus;
	}

	public String getIpPortGateWay10102() {
		return ipPortGateWay10102;
	}

	public void setIpPortGateWay10102(String ipPortGateWay10102) {
		this.ipPortGateWay10102 = ipPortGateWay10102;
	}

	public String getIpPortTLPro() {
		return ipPortTLPro;
	}

	public void setIpPortTLPro(String ipPortTLPro) {
		this.ipPortTLPro = ipPortTLPro;
	}

	public String getSmsServerURL() {
		return smsServerURL;
	}

	public void setSmsServerURL(String smsServerURL) {
		this.smsServerURL = smsServerURL;
	}

	public String getCommonRefundWebview() {
		return commonRefundWebview;
	}

	public void setCommonRefundWebview(String commonRefundWebview) {
		this.commonRefundWebview = commonRefundWebview;
	}

	public String getIpPort10102() {
		return ipPort10102;
	}

	public void setIpPort10102(String ipPort10102) {
		this.ipPort10102 = ipPort10102;
	}

	public String getDbHost() {
		return dbHost;
	}

	public void setDbHost(String dbHost) {
		this.dbHost = dbHost;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public EnvType getEnvType() {
		return envType;
	}

	public void setEnvType(EnvType envType) {
		this.envType = envType;
	}
	
	
}
