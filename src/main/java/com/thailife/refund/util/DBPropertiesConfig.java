package com.thailife.refund.util;

import com.thailife.refund.exception.CommonRefundException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DBPropertiesConfig {

	private static DBPropertiesConfig instance;
	
	private String env;
	
	private String url;
	
	private String username;
	
	private String password;
	
	private DBPropertiesConfig() throws CommonRefundException {
		
		 try (InputStream input = DBPropertiesConfig.class.getClassLoader().getResourceAsStream("database.config.properties")) {

			 Properties prop = new Properties();

			 if (input == null)
                throw new CommonRefundException("Sorry, unable to find config.properties");

			 prop.load(input);

			 env = prop.getProperty("env");
			 url = prop.getProperty("connection.url");
			 username = prop.getProperty("connection.username");
			 password = prop.getProperty("connection.password");
		 } 
		 catch (IOException ex) {
			 throw new CommonRefundException(ex);
		 }
	}

	public static DBPropertiesConfig getInstance() throws CommonRefundException {
		
		if (instance == null)
			instance = new DBPropertiesConfig();

		return instance;
	}


	@Override
	public String toString() {
		return "DBPropertiesConfig{" +
				"env='" + env + '\'' +
				", url='" + url + '\'' +
				", username='" + username + '\'' +
				", password='" + password + '\'' +
				'}';
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
