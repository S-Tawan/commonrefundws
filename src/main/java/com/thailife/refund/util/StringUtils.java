package com.thailife.refund.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.thailife.refund.constant.VariableConstant;
import com.thailife.refund.exception.CommonRefundException;

public class StringUtils {
	
	private StringUtils() throws CommonRefundException {
		throw new CommonRefundException(VariableConstant.MSG_UTILITYCLASS.getValue());
	}

	public static boolean isNull(String val) {
		
		boolean result = false;
		if (val == null || "".equals(val))
			result = true;
		
		return result;
	}

	public static String dateToString(Date date, SimpleDateFormat dateFORMATPOSTGRESQL) {
		return dateFORMATPOSTGRESQL.format(date);
	}

}
