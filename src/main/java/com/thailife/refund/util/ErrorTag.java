package com.thailife.refund.util;

public enum ErrorTag {
	
	ERROR("{}, {}");
	
	private String value;
	
	private ErrorTag(String status) {
		this.value = status;
	}
	
	public String getValue() {
		return value;
	}
}
