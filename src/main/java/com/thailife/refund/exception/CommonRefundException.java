package com.thailife.refund.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

public class CommonRefundException extends Exception {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String messageError;
	
	public CommonRefundException(String errorMessage) {
		super(errorMessage);
		messageError = errorMessage;
    }
	
	public CommonRefundException(Throwable cause) {
		super(cause);
		
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		cause.printStackTrace(pw);
		messageError = sw.toString();
	}

	public String getMessageError() {
		return messageError;
	}
	
	
}