package com.thailife.refund.exception;

import java.util.Date;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import com.thailife.refund.constant.VariableConstant;
import com.thailife.refund.models.DataServiceResponseM;
import com.thailife.refund.models.HeaderData;
import com.thailife.refund.models.ResponseStatus;
import com.thailife.refund.util.SimpleDateFormatUtil;
import org.slf4j.MDC;


public class DefaultExceptionHandle implements ExceptionMapper<CommonRefundException> {

	@Override
	public Response toResponse(CommonRefundException arg0) {
		
		DataServiceResponseM serviceResponse = new DataServiceResponseM();
		
		HeaderData headerData = new HeaderData();
		headerData.setuID(MDC.get(VariableConstant.SESSION_ID.getValue()));
		headerData.setResponseDateTime(SimpleDateFormatUtil.getDateFormatHeader().format(new Date()));
		headerData.setSentDateTime(SimpleDateFormatUtil.getDateFormatHeader().format(new Date()));

		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setErrorCode("E");
		responseStatus.setErrorMessage(arg0.getMessageError());
		responseStatus.setStatus("ERROR");
		
		serviceResponse.setHeaders(headerData);
		serviceResponse.setStatus(responseStatus);
		
		return Response.status(Status.OK).entity(serviceResponse).build();
	}
}
