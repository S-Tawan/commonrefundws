package com.thailife.refund.connection;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thailife.refund.constant.EnvType;
import com.thailife.refund.exception.CommonRefundException;
import com.thailife.refund.util.PropertiesConfig;

import utility.database.ConnectionBean;
import utility.database.DBConnectionFile;


/**
 * 
 * @author kitti.wug
 *
 */
public class DataBaseConnector {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DataBaseConnector.class);
	
	private static DataBaseConnector instance = null;
	
	private ConnectionBean connectionBean = null;
	
	private DataBaseConnector() {}
	
	public static DataBaseConnector getInstance() {
		if (instance == null)
			instance = new DataBaseConnector();
		 
		return instance;
	}
	
	public Connection getConnectionDB() throws CommonRefundException {
		
		try {
			
			if (EnvType.LOCALHOST.equals(PropertiesConfig.getInstance().getEnvType())) {
				
				String url = String.format("jdbc:postgresql://%s:5432/customer?currentSchema=common_refund", PropertiesConfig.getInstance().getDbHost());
				LOGGER.debug(url);
				
				Class.forName("org.postgresql.Driver");
				return DriverManager.getConnection(url, PropertiesConfig.getInstance().getDbUsername(), PropertiesConfig.getInstance().getDbPassword());
			}
			else {
				if (connectionBean == null) {
					
					File fileEncrypted = new File("/c/resources/db/dbconfig.cfg");
					File encryptKey = new File("/c/resources/db/dbkey.cfg");
					
					LOGGER.debug("DataBaseConnector : fileEncrypted = {}", fileEncrypted.getAbsolutePath());
					
					DBConnectionFile fileDB = new DBConnectionFile(fileEncrypted, encryptKey, "DBCustomer");
					connectionBean = fileDB.getConnectionBean();
				}
				
				String url = String.format("jdbc:postgresql://%s:5432/customer?currentSchema=common_refund", connectionBean.getHostName());
				LOGGER.debug(url);
				
				Class.forName("org.postgresql.Driver");
				return DriverManager.getConnection(url, connectionBean.getUserName(), connectionBean.getPassword());
			}
		}
		catch (Exception e) {
			LOGGER.error("DataBaseConnector ERROR : C");
			throw new CommonRefundException(e);
		}
	}

}
