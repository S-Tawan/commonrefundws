package com.thailife.tlafter.paymentcallback;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class ManageService {
	public String callWS(String url_ws, String request) throws Exception {
		URL url = new URL(url_ws);
		byte[] postData = request.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
		connection.setUseCaches(false);
		connection.setRequestProperty("charset", "utf-8");

		return callWS(connection, request);
	}

	private String callWS(HttpURLConnection connection, String request) throws Exception {
		OutputStream os = null;
		BufferedReader br = null;
		try {
			os = connection.getOutputStream();
			os.write(request.getBytes("UTF-8"));

			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK
					|| connection.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
				;// donothing
			} else {
				br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				String tmp = "", output = "";
				while ((tmp = br.readLine()) != null) {
					output += tmp;
				}
				throw new Exception(
						"Failed : HTTP error code : " + connection.getResponseCode() + "\n, Error : " + output);
			}

			br = new BufferedReader(new InputStreamReader((connection.getInputStream()), "UTF-8"));
			String tmp = "", output = "";
			while ((tmp = br.readLine()) != null) {
				output += tmp;
			}
			return output;
		} catch (Exception e) {
//        	log.error(ErrorTag.ERROR.getValue(), e.getMessage(), e);
			throw e;
		} finally {
			if (os != null)
				os.close();
			if (br != null)
				br.close();
			if (connection != null)
				connection.disconnect();
		}
	}
	
	public String callTo() throws Exception {
		String test = "http://sit-sgi-api.thailife.com:8280";
		String callTo = "http://sgi-api.thailife.com:8280";// default
//		Properties prop = new Properties();
		try {
//			InputStream input = ManageService.class.getClassLoader().getResourceAsStream("config.properties");
//			if (input == null)
//				return callTo;
//			prop.load(input);
//			String ip = prop.getProperty("tws.thailife.ipport");

			final DatagramSocket socket = new DatagramSocket();
				  socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
				  String ip = socket.getLocalAddress().getHostAddress();
				  socket.close();
			if (ip.contains("206.1.1"))
				callTo = test;
			else if (ip.contains("10.102.60"))
				callTo = "http://dev-sgi-api.thailife.com:8280";
			else if (ip.contains("10.102.63"))
				callTo = "http://sit-sgi-api.thailife.com:8280";
			else if (ip.contains("10.102.65"))
				callTo = "http://uat-sgi-api.thailife.com:8280";
			else if (ip.contains("10.102.45"))
				callTo = "http://pre-sgi-api.thailife.com:8280";
			else
				callTo = "http://sgi-api.thailife.com:8280";
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return callTo;
	}

}
