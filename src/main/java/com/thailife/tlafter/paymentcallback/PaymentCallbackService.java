package com.thailife.tlafter.paymentcallback;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.ObjectMapper;

import com.thailife.tlafter.paymentcallback.ManageService;
import org.apache.log4j.Logger;

@Path("/")
public class PaymentCallbackService {
	final static Logger logger = Logger.getLogger(PaymentCallbackService.class);
	@POST
	@Path("/paymentCallback")
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String paymentCallback(String objectId, String token, String status, String message, String saveCard) {
		ManageService manageService = new ManageService();
		Map<String, Map> request = new HashMap();
		Map<String, String> headerData = new HashMap();
		Map<String, String> requestRecord = new HashMap();
		headerData.put("messageId", "");
		headerData.put("sentDateTime", "");
		String[] param = objectId.split("&");
		if (param.length == 5) {
			requestRecord.put("objectId", param[0].split("=")[1]);
			requestRecord.put("token", param[1].split("=")[1]);
			requestRecord.put("status", param[2].split("=")[1]);
			requestRecord.put("saveCard", param[4].split("=")[1]);
		}
		request.put("headerData", headerData);
		request.put("requestRecord", requestRecord);

		ObjectMapper objectMapper = new ObjectMapper();

		String json = "{}";
		String bcp05 = "Fail";
		String url = "";
		try {
			url = manageService.callTo() + "/KBankCallback/rest/creditcallback/tlafterplus";
			json = objectMapper.writeValueAsString(request);
			bcp05 = manageService.callWS(url, json);
			logger.info("#paymentCallback : " + json);
			logger.info("#result : " + bcp05);
		} catch (Exception e) {
			System.out.println("#" + url);
			System.out.println("#paymentCallback false : " + json);
			logger.info("#" + url);
			logger.info("#paymentCallback false : " + json);
		}
		return bcp05; // "it's ok " + objectId + "#" + token + "#" + status + "#" + message + "#" +
						// saveCard+">"+bcp05+"\n"+json;
	}
}
